@extends('layouts.app')

@section('title')
{{ __('auth.register.title') }}
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
      <form method="POST" action="{{ route('register') }}" aria-label="{{ __('auth.register.title') }}" id="registerForm">
        <div class="card" data-background="color" data-color="blue">
          <div class="card-header">
            <h3 class="card-title">{{ __('auth.register.title') }}</h3>
          <a href="/" class="logo"><img src="/img/{{ env('IMAGE_ASSET_DIRECTORY') }}/white.png"/></a>
          </div>
          <div class="card-content">
            @csrf
            <div class="form-group">
              <label>{{ __('auth.register.name_label') }}</label>
              <input id="name" type="text" name="name" value="{{ old('name') }}"
                class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} input-no-border" required autofocus>
              @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group">
              <label>{{ __('auth.register.email_label') }}</label>
              <input id="email" type="email" name="email" value="{{ old('email') }}"
                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-no-border" email="true" autocomplete="off" required>
                @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group">
              <label>{{ __('auth.register.password_label') }}</label>
              <input id="password" type="password" name="password"
                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-no-border" required>
              @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group">
              <label>{{ __('auth.register.confirm_password_label') }}</label>
              <input id="password-confirm" type="password" class="form-control input-no-border" name="password_confirmation" required>
            </div>
          </div>
          <div class="card-footer text-center">
            <button type="submit" class="btn btn-fill btn-wd">{{ __('auth.register.button') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  .login-page > .content, .lock-page > .content{
    padding-top: 20vh;
  }
</style>
@endsection
