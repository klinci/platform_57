@extends('layouts.app')

@section('title')
{{ __('auth.login.title') }}
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3" style="padding-left:0px;padding-right:0px;">
      @if ($errors->has('email'))
        <div class="alert alert-danger" role="alert">
        {{ $errors->first('email') }}
        </div>
      @endif
      <form method="POST" action="{{ route('login') }}" aria-label="{{ __('auth.login.title') }}" id="loginForm">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">{{ __('auth.login.title') }}</h3>
            <a href="/" class="logo"><img src="/img/{{ env('IMAGE_ASSET_DIRECTORY') }}/white.png"/></a>
          </div>
          <div class="card-content">
            @csrf
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
              <input id="email" type="email" name="email" value="{{ old('email') }}"
                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-no-border"
                placeholder="{{ __('auth.login.email_placeholder') }}" email="true" autocomplete="off" autofocus required>
            </div>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-no-border" name="password" placeholder="{{ __('auth.login.password_placeholder') }}" required>
            </div>
              @if ($errors->has('password'))
              <div class="input-group" style="margin-top: -10px;">
                <span class="input-group-addon" id="basic-addon1" style="width:45px;"></span>
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              </div>
              @endif
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-fill btn-wd">{{ __('auth.login.button') }}</button>
            <div class="forgot pull-left" >
              <a href="{{ route('password.request') }}">{{ __('auth.login.forgot_password') }}</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<style type="text/css">
  .input-group-focus .input-group-addon{
    background: none;
  }
</style>
@endsection

@section('script')
@endsection
