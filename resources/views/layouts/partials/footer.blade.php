<footer class="footer footer-transparent">
  <div class="container">
    <div class="copyright">
      &copy; <script>document.write(new Date().getFullYear())</script>, {{ env('APP_NAME') }}.
    </div>
  </div>
</footer>
