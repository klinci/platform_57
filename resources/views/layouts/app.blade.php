<!doctype html>
<html lang="en">
  <head>
    @include('layouts.partials.htmlheader')
  </head>
  <body>
    @include('layouts.partials.navbar')
    <div class="wrapper wrapper-full-page">
      <!-- You can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
      <div class="full-page login-page" data-color="none" data-image="">
        <div class="content">
          @yield('content')
        </div>
        @include('layouts.partials.footer')
      </div>
    </div>
  </body>
  @include('layouts.partials.script')
</html>
