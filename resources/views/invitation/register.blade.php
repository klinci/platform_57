@extends('layouts.app')

@section('title')
{{ __('auth.register.title') }}
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
      <form method="POST" action="{{ url('invitation/register') }}" aria-label="{{ __('auth.register.title') }}" id="registerForm">
        <input type="hidden" name="destination" value="{{ $invitation->destination }}"/>
        <input type="hidden" name="destination_id" value="{{ $invitation->destination_id }}"/>
        <input type="hidden" name="email" value="{{ $invitation->email }}"/>
        <div class="card" data-background="color" data-color="blue">
          <div class="card-header">
            <a href="/" class="logo"><img src="/img/{{ env('IMAGE_ASSET_DIRECTORY') }}/white.png"/></a>
          </div>
          <div class="card-content">
            @csrf
            <div class="form-group">
              <label>{{ __('auth.register.name_label') }}</label>
              <input id="name" type="text" name="name" value="{{ $invitation->email }}"
                class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} input-no-border" required autofocus>
              @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group">
                <label>Workspace</label>
                <input id="workspace" type="text" name="workspace" value="{{ $invitation->workspace->name }}" class="form-control" disabled>
              </div>
            <div class="form-group">
              <label>{{ __('auth.register.email_label') }}</label>
              <input id="email" type="email" name="email" value="{{ $invitation->email }}" class="form-control input-no-border" disabled>
            </div>
            <div class="form-group">
              <label>{{ __('auth.register.password_label') }}</label>
              <input id="password" type="password" name="password"
                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-no-border" required>
              @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>
          </div>
          <div class="card-footer text-center">
            <button type="submit" class="btn btn-fill btn-wd">Continue</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  .login-page > .content, .lock-page > .content{
    padding-top: 20vh;
  }
</style>
@endsection
