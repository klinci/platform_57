import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
import Overview from '../components/Dashboard//Overview.vue'
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

/**
 * User
 */
import UserMyProfile from '../components/Dashboard/Views/User/UserMyProfile.vue'
import UserList from '../components/Dashboard/Views/User/UserList.vue'
import UserEdit from '../components/Dashboard/Views/User/UserEdit.vue'
import UserCreate from '../components/Dashboard/Views/User/UserCreate.vue'

/**
 * Access Control
 */
import AccessControlOverview from '../components/Dashboard/Views/AccessControl/AccessControlOverview.vue'

/**
 * Access Control Role
 */
import AccessControlRoleList from '../components/Dashboard/Views/AccessControl/Role/AccessControlRoleList.vue'
import AccessControlRoleEdit from '../components/Dashboard/Views/AccessControl/Role/AccessControlRoleEdit.vue'
import AccessControlRoleCreate from '../components/Dashboard/Views/AccessControl/Role/AccessControlRoleCreate.vue'

/**
 * Access Control Permission
 */
import AccessControlPermissionList from '../components/Dashboard/Views/AccessControl/Permission/AccessControlPermissionList.vue'
import AccessControlPermissionEdit from '../components/Dashboard/Views/AccessControl/Permission/AccessControlPermissionEdit.vue'
import AccessControlPermissionCreate from '../components/Dashboard/Views/AccessControl/Permission/AccessControlPermissionCreate.vue'

/**
 * Media Library
 */
import MediaLibraryList from '../components/Dashboard/Views/MediaLibrary/MediaLibraryList.vue'

/**
 * Workspace
 */
import WorkspaceOverview from '../components/Dashboard/Views/Workspace/WorkspaceOverview.vue'

/**
 * Team
 */
import TeamOverview from '../components/Dashboard/Views/Team/TeamOverview.vue'

/**
 * Project
 */
import ProjectOverview from '../components/Dashboard/Views/Project/ProjectOverview.vue'

/**
 * Campaign
 */
import CampaignOverview from '../components/Dashboard/Views/Campaign/CampaignOverview.vue'
import CampaignMinisite from '../components/Dashboard/Views/Campaign/CampaignMinisite.vue'
import CampaignMinisiteEditor from '../components/Dashboard/Views/Campaign/CampaignMinisiteEditor.vue'
import CampaignCreate from '../components/Dashboard/Views/Campaign/CampaignCreate.vue'
import QuizCampaignCreate from '../components/Dashboard/Views/Campaign/Quiz/QuizCreate.vue'
import ResultCreate from '../components/Dashboard/Views/Campaign/Result/ResultCreate.vue'


/**
 * Bootstrap
 */
//import BootstrapOverview from './../../../../Modules/Bootstrap/Resources/assets/js/components/BootstrapOverview.vue'

/**
 * Integration
 */
import IntegrationList from './../../../../Modules/Integration/Resources/assets/js/components/IntegrationList.vue'
import IntegrationCreate from './../../../../Modules/Integration/Resources/assets/js/components/IntegrationCreate.vue'
import IntegrationEdit from './../../../../Modules/Integration/Resources/assets/js/components/IntegrationEdit.vue'

/**
 * SMTP
 */
import SMTPList from './../../../../Modules/SMTP/Resources/assets/js/components/SMTPList.vue'
import SMTPCreate from './../../../../Modules/SMTP/Resources/assets/js/components/SMTPCreate.vue'
import SMTPEdit from './../../../../Modules/SMTP/Resources/assets/js/components/SMTPEdit.vue'


let dashboardPage = {
  path: '/dashboard',
  component: DashboardLayout,
  redirect: '/dashboard',
  children: [
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Overview
    }
  ]
}

let userMyProfilePage = {
  path: '/profile',
  component: DashboardLayout,
  children: [
    {
      path: '/profile',
      name: 'My Profile',
      component: UserMyProfile
    }
  ]
}

let usersPage = {
  path: '/admin/user',
  component: DashboardLayout,
  children: [
    {
      path: '/admin/user',
      name: 'Users',
      component: UserList
    },
    {
      path: '/admin/user/create',
      name: 'Create User',
      component: UserCreate
    },
    {
      path: '/admin/user/edit/:id',
      name: 'Edit User',
      component: UserEdit
    }
  ]
}

let accessControlPage = {
  path: '/admin/access-control',
  component: DashboardLayout,
  children: [
    {
      path: '/admin/access-control/overview',
      name: 'Access Control',
      component: AccessControlOverview
    }
  ]
}

let accessControlRolePage = {
  path: '/admin/access-control',
  component: DashboardLayout,
  children: [
    {
      path: '/admin/access-control/role',
      name: 'Roles',
      component: AccessControlRoleList
    },
    {
      path: '/admin/access-control/role/create',
      name: 'Create Role',
      component: AccessControlRoleCreate
    },
    {
      path: '/admin/access-control/role/edit/:id',
      name: 'Edit Role',
      component: AccessControlRoleEdit
    }
  ]
}

let accessControlPermissionPage = {
  path: '/admin/access-control',
  component: DashboardLayout,
  children: [
    {
      path: '/admin/access-control/permission',
      name: 'Permissions',
      component: AccessControlPermissionList
    },
    {
      path: '/admin/access-control/permission/create',
      name: 'Create Permission',
      component: AccessControlPermissionCreate
    },
    {
      path: '/admin/access-control/permission/edit/:id',
      name: 'Edit Permission',
      component: AccessControlPermissionEdit
    }
  ]
}

let mediaLibraryPage = {
  path: '/media-library',
  component: DashboardLayout,
  children: [
    {
      path: '/media-library',
      name: 'Media Library',
      component: MediaLibraryList
    }
  ]
}

let workspaceTeamProjectPage = {
  path: '/workspace',
  component: DashboardLayout,
  children: [
    {
      path: '/workspace/:workspaceid',
      name: 'Workspace Overview',
      component: WorkspaceOverview
    },
    {
      path: '/workspace/:workspaceid/team/:teamid',
      name: 'Team Overview',
      component: TeamOverview
    },
    {
      path: '/workspace/:workspaceid/team/:teamid/project/:projectid/campaigns',
      name: 'Campaign Overview',
      component: CampaignOverview
    },
    {
      path: '/workspace/:workspaceid/team/:teamid/project/:projectid/campaign/:campaignid',
      name: 'Campaign Minisite',
      component: CampaignMinisite
    },
    {
      path: '/workspace/:workspaceid/team/:teamid/project/:projectid/campaign/:campaignid/designs',
      name: 'campaign-minisite-editor',
      component: CampaignMinisiteEditor
    },
    {
      path: '/create',
      name: 'Create Campaign',
      component: CampaignCreate
    },
    {
      path: '/edit-quiz',
      name: 'Create Campaign Quiz',
      component: QuizCampaignCreate
    },
    {
      path: '/edit-result',
      name: 'Edit Result',
      component: ResultCreate
    }
  ]
}

let integrationPage = {
  path: '/integration',
  component: DashboardLayout,
  children: [
    {
      path: '/integration',
      name: 'Integration List',
      component: IntegrationList
    },
    {
      path: '/integration/create',
      name: 'Create Integration',
      component: IntegrationCreate
    },
    {
      path: '/integration/edit/:integrationid',
      name: 'Edit Integration',
      component: IntegrationEdit
    }
  ]
}

let smtpPage = {
  path: '/smtp',
  component: DashboardLayout,
  children: [
    {
      path: '/smtp',
      name: 'SMTP List',
      component: SMTPList
    },
    {
      path: '/smtp/create',
      name: 'Create SMTP',
      component: SMTPCreate
    },
    {
      path: '/smtp/edit/:smtpid',
      name: 'Edit SMTP',
      component: SMTPEdit
    }
  ]
}

/*let bootstrapPage = {
  path: '/',
  component: DashboardLayout,
  redirect: '/bootstrap/overview',
  children: [
    {
      path: '/bootstrap/overview',
      name: 'Bootstrap Overview',
      component: BootstrapOverview
    }
  ]
}*/

const routes = [
  dashboardPage,
  userMyProfilePage,
  usersPage,
  accessControlPage,
  accessControlRolePage,
  accessControlPermissionPage,
  mediaLibraryPage,
  workspaceTeamProjectPage,
  integrationPage,
  smtpPage,
  //bootstrapPage,
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        component: Overview
      }
    ]
  },
  {path: '*', component: NotFound}
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
