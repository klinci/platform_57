<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Akun tidak ditemukan.',
    'throttle' => 'Terlalu banyak permintaan login. Silahkan coba lagi dalam :seconds detik.',
    'login' => [
        'title' => 'Masuk',
        'register' => 'Daftar',
        'email_placeholder' => 'Masukkan email',
        'password_placeholder' => 'Kata sandi',
        'forgot_password' => 'Lupa password?',
        'button' => 'Masuk',
    ],
    'register' => [
        'title' => 'Daftar',
        'name_label' => 'Nama',
        'email_label' => 'Alamat email',
        'password_label' => 'Kata sandi',
        'confirm_password_label' => 'Konfirmasi kata sandi',
        'button' => 'Daftar',
    ],
    'forgot_password' => [
        'title' => 'Atur ulang kata sandi',
        'email_label' => 'Alamat email',
        'button' => 'Kirim tautan',
    ],
    'reset_password' => [
        'title' => 'Atur ulang kata sandi',
        'email_label' => 'Alamat email',
        'password_label' => 'Kata sandi',
        'confirm_password_label' => 'Konfirmasi kata sandi',
        'button' => 'Atur Ulang Password',
    ],
];
