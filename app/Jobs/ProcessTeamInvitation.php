<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Invitation;
use App\Team;
use App\Notifications\TeamInvitation;

class ProcessTeamInvitation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invitation;
    protected $team;
    protected $isRegisteredUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation, Team $team, $isRegisteredUser)
    {
        $this->invitation = $invitation;
        $this->team = $team;
        $this->isRegisteredUser = $isRegisteredUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->invitation->notify(new TeamInvitation($this->team, $this->invitation, $this->isRegisteredUser));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
