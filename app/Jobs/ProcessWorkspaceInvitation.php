<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Invitation;
use App\Workspace;
use App\Notifications\WorkspaceInvitation;

class ProcessWorkspaceInvitation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invitation;
    protected $workspace;
    protected $isRegisteredUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation, Workspace $workspace, $isRegisteredUser)
    {
        $this->invitation = $invitation;
        $this->workspace = $workspace;
        $this->isRegisteredUser = $isRegisteredUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->invitation->notify(new WorkspaceInvitation($this->workspace, $this->invitation, $this->isRegisteredUser));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
