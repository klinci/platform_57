<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Invitation;
use App\Project;
use App\Notifications\ProjectInvitation;

class ProcessProjectInvitation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invitation;
    protected $project;
    protected $isRegisteredUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation, Project $project, $isRegisteredUser)
    {
        $this->invitation = $invitation;
        $this->project = $project;
        $this->isRegisteredUser = $this->isRegisteredUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->invitation->notify(new ProjectInvitation($this->project, $this->invitation, $this->isRegisteredUser));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
