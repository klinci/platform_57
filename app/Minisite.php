<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Minisite extends Model
{
	use SoftDeletes;

    protected $table = 'minisites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'campaign_id',
        'name',
        'sort',
        'type',
        'first',
        'force_redirect',
        'url',
        'is_protected_by_optin',
        'optin',
        'header',
        'content',
        'footer',
        'next',
        'campaign_target_id',
        'styles'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Campaign
     * @return Collection
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Widgets
     * @return Collection
     */
    public function widgets()
    {
        return $this->hasMany(Widget::class);
    }
}
