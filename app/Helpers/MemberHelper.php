<?php

namespace App\Helpers;

use Illuminate\Support\Collection;
use App\{Workspace, Team, Project, Campaign};
use App\Hashers\UserHasher;

class MemberHelper
{
    /**
     * Get workspace members
     * @param Workspace
     * @return Collection
     */
    public static function workspace(Workspace $workspace) : Collection
    {
        return self::members($workspace);
    }

    /**
     * Get team members
     * @param Team
     * @return Collection
     */
    public static function team(Team $team) : Collection
    {
        return self::members($team);
    }

    /**
     * Get project members
     * @param Project
     * @return Collection
     */
    public static function project(Project $project) : Collection
    {
        return self::members($project);
    }

    /**
     * Get campaign members
     * @param Campaign
     * @return Collection
     */
    public static function campaign(Campaign $campaign) : Collection
    {
        return self::members($campaign);
    }

    /**
     * Get members for eloquent model
     * @return Collection
     */
    public static function members($model) : Collection
    {
        $members = collect();

        $model->permissions->each(function ($permission) use ($members) {
            $member = new \stdClass();
            $member->id = UserHasher::encode($permission->user->id);
            $member->name = $permission->user->name;
            $member->email = $permission->user->email;
            $member->role = $permission->role->name;

            $members->push($member);
        });

        return $members;
    }
}