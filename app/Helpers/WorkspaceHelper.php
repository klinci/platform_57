<?php

namespace App\Helpers;

use Illuminate\Support\Collection;
use App\User;

class WorkspaceHelper
{
    /**
     * Workspaces
     * Return all workspaces belongs to user
     * @param @userId
     * @return Collection
     */
    public static function workspaces($userId) : Collection
    {
        $user = User::findOrFail($userId);
        $workspaces = collect();

        /**
         * Find Workspaces
         */
        $_workspaces = $user->workspaces;
        $_workspaces->each(function ($permission) use ($workspaces) {
            if (isset($permission->workspace)) {
                $workspaces->push($permission->workspace);
            }
        });

        /**
         * Find Workspace by team
         */
        $teams = $user->teams;
        $teams->each(function ($permission) use ($workspaces) {
            if (isset($permission->team->workspace)) {
                $workspaces->push($permission->team->workspace);
            }
        });

        /**
         * Find workspace by project
         */
        $projects = $user->projects;
        $projects->each(function ($permission) use ($workspaces) {
            if (isset($permission->project->team->workspace)) {
                $workspaces->push($permission->project->team->workspace);
            }
        });

        /**
         * Find workspace by campaign
         */
        $campaigns = $user->campaigns;
        $campaigns->each(function ($permission) use ($workspaces) {
            if (isset($permission->campaign->project->team->workspace)) {
                $workspaces->push($permission->campaign->project->team->workspace);
            }
        });

        return $workspaces->unique();
    }
}