<?php

namespace App\Helpers;

use App\WorkspacePermission;
use App\Team;
use App\TeamPermission;
use App\Project;
use App\ProjectPermission;
use App\Campaign;
use App\CampaignPermission;

class RevokePermission
{
    /**
     * Workspace
     * Revoke Permission
     * @param $workspaceId, $userId
     * @return null
     */
    public static function revokePermissionFromWorkspace($workspaceId, $userId)
    {
        /**
         * Unassign permission from workspace
         */
        self::unassignPermissionFromWorkspace($workspaceId, $userId);

        /**
         * Unassign permission from teams
         */
        self::unassignPermissionFromTeams($workspaceId, $userId);
    }

    /**
     * Team
     * Revoke Permission
     * @param $teamId, $userId
     * @return null
     */
    public static function revokePermissionFromTeam($teamId, $userId)
    {
        /**
         * Unassign permission from team
         */
        self::unassignPermissionFromTeam($teamId, $userId);

        /**
         * Unassign permission from projects
         */
        self::unassignPermissionFromProjects($teamId, $userId);
    }

    /**
     * Project
     * Revoke Permission
     * @param $projectId, $userId
     */
    public static function revokePermissionFromProject($projectId, $userId)
    {
        /**
         * Unassign permission from project
         */
        self::unassignPermissionFromProject($projectId, $userId);

        /**
         * Unassign permission from campaigns
         */
        self::unassignPermissionFromCampaigns($projectId, $userId);
    }

    /**
     * Campaign
     * Revoke Permission
     * @param $campaignId, $userId
     * @return null
     */
    public static function revokePermissionFromCampaign($campaignId, $userId)
    {
        /**
         * Unassign permission from campaign
         */
        self::unassignPermissionFromCampaign($campaignId, $userId);
    }

    /**
     * Unassign permission from teams
     * @param $workspaceId, $userId
     * @return null
     */
    public static function unassignPermissionFromTeams($workspaceId, $userId)
    {
        /**
         * Look for teams belongs to the workspace
         */
        $teams = Team::whereWorkspaceId($workspaceId)->get();
        $teams->each(function ($team) use ($userId) {
            /**
             * Unassign permission from team
             */
            self::unassignPermissionFromTeam($team->id, $userId);

            /**
             * Unassign permission from projects
             */
            self::unassignPermissionFromProjects($team->id, $userId);
        });
    }

    /**
     * Unassign permission from projects
     * @param $teamId, $userId
     * @return null
     */
    public static function unassignPermissionFromProjects($teamId, $userId)
    {
        /**
         * Look for projects belongs to the team
         */
        $projects = Project::whereTeamId($teamId)->get();
        $projects->each(function ($project) use ($userId) {
            /**
             * Unassign permission from project
             */
            self::unassignPermissionFromProject($project->id, $userId);

            /**
             * Unassign permission from campaigns
             */
            self::unassignPermissionFromCampaigns($project->id, $userId);
        });
    }

    /**
     * Unassign permission from campaigns
     * @param $projectId, $userId
     * @return null
     */
    public static function unassignPermissionFromCampaigns($projectId, $userId)
    {
        /**
         * Look for campaigns belongs to the project
         */
        $campaigns = Campaign::whereProjectId($projectId)->get();
        $campaigns->each(function ($campaign) use ($userId) {
            /**
             * Unassign permission form campaign
             */
            self::unassignPermissionFromCampaign($campaign->id, $userId);
        });
    }

    /**
     * Unassign permission from workspace
     * @param $workspaceId, $userId
     * @return null
     */
    public static function unassignPermissionFromWorkspace($workspaceId, $userId)
    {
        WorkspacePermission::whereWorkspaceId($workspaceId)->whereUserId($userId)->delete();
    }

    /**
     * Unassign permission from team
     * @param $teamId, $userId
     * @return null
     */
    public static function unassignPermissionFromTeam($teamId, $userId)
    {
        TeamPermission::whereTeamId($teamId)->whereUserId($userId)->delete();
    }

    /**
     * Unassign permission from project
     * @param $projectId, $userId
     * @return null
     */
    public static function unassignPermissionFromProject($projectId, $userId)
    {
        ProjectPermission::whereProjectId($projectId)->whereUserId($userId)->delete();
    }

    /**
     * Unassign permission from campaign
     * @param $campaignId, $userId
     * @return null
     */
    public static function unassignPermissionFromCampaign($campaignId, $userId)
    {
        CampaignPermission::whereCampaignId($campaignId)->whereUserId($userId)->delete();
    }
}