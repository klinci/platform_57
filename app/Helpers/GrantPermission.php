<?php

namespace App\Helpers;

use App\WorkspacePermission;
use App\Team;
use App\TeamPermission;
use App\Project;
use App\ProjectPermission;
use App\Campaign;
use App\CampaignPermission;

class GrantPermission
{
    /**
     * Workspace
     * Grant Permission
     * @param $workspaceId, $userId
     * @return null
     */
    public static function grantPermissionToWorkspace($workspaceId, $userId)
    {
        /**
         * Assign permission to workspace
         */
        self::assignPermissionToWorkspace($workspaceId, $userId);

        /**
         * Assign permission to the teams that belongs to the workspace
         */
        self::assignPermissionToTeams($workspaceId, $userId);
    }

    /**
     * Team
     * Grant Permission
     * @param $teamId, $userId
     * @return null
     */
    public static function grantPermissionToTeam($teamId, $userId)
    {
        /**
         * Assign permission to team
         */
        self::assignPermissionToTeam($teamId, $userId);

        /**
         * Assign user to the projects that belongs to the team
         */
        self::assignPermissionToProjects($teamId, $userId);
    }

    /**
     * Project
     * Grant Permission
     * @param $projectId, $userId
     * @return null
     */
    public static function grantPermissionToProject($projectId, $userId)
    {
        /**
         * Assign permission to project
         */
        self::assignPermissionToProject($projectId, $userId);

        /**
         * Assign permission to the campaigns that belongs to the project
         */
        self::assignPermissionToCampaigns($projectId, $userId);
    }

    /**
     * Campaign
     * Grant Permission
     * @param $campaignId, $userId
     * @return null
     */
    public static function grantPermissionToCampaign($campaignId, $userId)
    {
        /**
         * Assign permission to campaign
         */
        self::assignPermissionToCampaign($campaignId, $userId);
    }

    /**
     * Assign permission to teams
     * @param $workspaceId, $userId
     * @return null
     */
    public static function assignPermissionToTeams($workspaceId, $userId)
    {
        /**
         * Look for teams belongs to the workspace
         */
        $teams = Team::whereWorkspaceId($workspaceId)->get();
        $teams->each(function ($team) use ($userId) {
            /**
             * Assign permission to team
             */
            self::assignPermissionToTeam($team->id, $userId);

            /**
             * Assign permission to projects
             */
            self::assignPermissionToProjects($team->id, $userId);
        });
    }

    /**
     * Assign permission to projects
     * @param @teamId, $userId
     * @return null
     */
    public static function assignPermissionToProjects($teamId, $userId)
    {
        /**
         * Look for projects belongs to the team
         */
        $projects = Project::whereTeamId($teamId)->get();
        $projects->each(function ($project) use ($userId) {
            /**
             * Assign permision to project
             */
            self::assignPermissionToProject($project->id, $userId);

            /**
             * Assign permission to campaigns
             */
            self::assignPermissionToCampaigns($project->id, $userId);
        });
    }

    /**
     * Assign permission to campaigns
     * @param $projectId, $userId
     * @return null
     */
    public static function assignPermissionToCampaigns($projectId, $userId)
    {
        /**
         * Look for campaigns belongs to the project
         */
        $campaigns = Campaign::whereProjectId($projectId)->get();
        $campaigns->each(function ($campaign) use ($userId) {
            /**
             * Assign permission to campaign
             */
            self::assignPermissionToCampaign($campaign->id, $userId);
        });
    }

    /**
     * Assign permission to workspace
     * @param $workspaceId, $userId
     * @return null
     */
    public static function assignPermissionToWorkspace($workspaceId, $userId)
    {
        WorkspacePermission::updateOrCreate([
            'workspace_id' => $workspaceId,
            'user_id' => $userId,
            'role_id' => env('MEMBER_ROLE_ID')
        ]);
    }

    /**
     * Assign user to the team
     * @param $teamId, $userId
     * @return null
     */
    public static function assignPermissionToTeam($teamId, $userId)
    {
        TeamPermission::updateOrCreate([
            'team_id' => $teamId,
            'user_id' => $userId,
            'role_id' => env('MEMBER_ROLE_ID')
        ]);
    }

    /**
     * Assign user to the project
     * @param $projectId, $userId
     * @return null
     */
    public static function assignPermissionToProject($projectId, $userId)
    {
        ProjectPermission::updateOrCreate([
            'project_id' => $projectId,
            'user_id' => $userId,
            'role_id' => env('MEMBER_ROLE_ID')
        ]);
    }

    /**
     * Assign user to the campaign
     * @param $campaignId, $userId
     * @return null
     */
    public static function assignPermissionToCampaign($campaignId, $userId)
    {
        CampaignPermission::updateOrCreate([
            'campaign_id' => $campaignId,
            'user_id' => $userId,
            'role_id' => env('MEMBER_ROLE_ID')
        ]);
    }
}