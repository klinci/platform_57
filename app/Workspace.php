<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workspace extends Model
{
    use SoftDeletes;

    protected $table = 'workspaces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    protected $visible = ['id', 'name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Permission
     * @return Collection
     */
    public function permission()
    {
        return $this->hasOne(WorkspacePermission::class);
    }

    /**
     * Permissions
     * @return Collection
     */
    public function permissions()
    {
        return $this->hasMany(WorkspacePermission::class);
    }

    /**
     * Teams
     * @return Collection
     */
    public function teams()
    {
        return $this->hasMany(Team::class);
    }
}
