<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\ProjectHasher;
use App\Project;

class ProjectTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Project $project)
    {
        return [
            'id' => ProjectHasher::encode($project->id),
            'name' => $project->name,
            'team' => fractal($project->team, TeamTransformer::class)->toArray()['data'],
            'created_at' => $project->created_at,
            'updated_at' => $project->updated_at
        ];
    }
}
