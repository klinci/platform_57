<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\TeamHasher;
use App\Team;

class TeamTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Team $team)
    {
        return [
            'id' => TeamHasher::encode($team->id),
            'name' => $team->name,
            'workspace' => fractal($team->workspace, WorkspaceTransformer::class)->toArray()['data'],
            'created_at' => $team->created_at,
            'updated_at' => $team->updated_at
        ];
    }
}
