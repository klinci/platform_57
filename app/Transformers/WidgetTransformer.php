<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\WidgetHasher;
use App\Widget;

class WidgetTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Widget $widget)
    {
        return [
            'id' => WidgetHasher::encode($widget->id),
            'minisite' => $widget->minisite,
            'type' => $widget->type,
            'target' => $widget->target,
            'content' => json_decode($widget->content)
        ];
    }
}
