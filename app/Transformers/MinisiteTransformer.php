<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Hashers\MinisiteHasher;
use App\Minisite;

class MinisiteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Minisite $minisite)
    {
        return [
            'id' => MinisiteHasher::encode($minisite->id),
            'campaign' => $minisite->campaign,
            'type' => $minisite->type,
            'name' => $minisite->name,
            'sort' => $minisite->sort,
            'first' => $minisite->first,
            'force_redirect' => $minisite->force_redirect,
            'url' => $minisite->url,
            'is_protected_by_optin' => $minisite->is_protected_by_optin,
            'optin' => $minisite->optin,
            'header' => json_decode($minisite->header),
            'content' => json_decode($minisite->content),
            'footer' => json_decode($minisite->content),
            'next' => $minisite->next,
            'campaign_target_id' => $minisite->campaign_target_id,
            'styles' => json_decode($minisite->styles)
        ];
    }
}
