<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class ProfileTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $profile)
    {
        return [
            'name' => $profile->name,
            'email' => $profile->email
        ];
    }
}
