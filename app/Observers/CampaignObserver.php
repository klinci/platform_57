<?php

namespace App\Observers;

use App\{Campaign, Minisite};
use Storage;

class CampaignObserver
{
    /**
     * Handle the campaign "created" event.
     *
     * @param  \App\Campaign  $campaign
     * @return void
     */
    public function created(Campaign $campaign)
    {
        $template = $campaign->template->slug;

        switch ($template) {
            case 'quiz':
                $defaultNames = collect(['blank' => 'Intro', 'quiz' => 'Quiz']);
                break;
            default:
                $defaultNames = collect(['blank' => 'Intro', 'quiz' => 'Quiz']);
                break;
        }

        $i = 1;

        $defaultNames->each(function ($item, $key) use ($campaign, &$i) {
            /**
             * Set type null if blank
             */
            $type = $key;
            if ($key == 'blank') {
                $type = null;
            }

            /**
             * Get from storage
             */
            $defaultMinisite = json_decode(Storage::get(sprintf('json/minisites/%s.json', $key)));

            /**
             * Modify some data
             */
            $defaultMinisite->name = $item;
            $defaultMinisite->type = $type;
            $defaultMinisite->sort = $i;
            $defaultMinisite->first = ($i === 1) ? true : false;
            $defaultMinisite->styles = json_encode($defaultMinisite->styles);
            $defaultMinisite->header = json_encode($defaultMinisite->header);
            $defaultMinisite->content = json_encode($defaultMinisite->content);
            $defaultMinisite->footer = json_encode($defaultMinisite->footer);

            /**
             * Convert to Collection
             */
            $defaultMinisite = collect($defaultMinisite);

            /**
             * Prepend campaign_id
             */
            $defaultMinisite->prepend($campaign->id, 'campaign_id');

            /**
             * Store Minisite
             */
            $minisite = new Minisite();
            $minisite->create($defaultMinisite->toArray());

            $i++;
        });
    }

    /**
     * Handle the campaign "updated" event.
     *
     * @param  \App\Campaign  $campaign
     * @return void
     */
    public function updated(Campaign $campaign)
    {
        //
    }

    /**
     * Handle the campaign "deleted" event.
     *
     * @param  \App\Campaign  $campaign
     * @return void
     */
    public function deleted(Campaign $campaign)
    {
        //
    }

    /**
     * Handle the campaign "restored" event.
     *
     * @param  \App\Campaign  $campaign
     * @return void
     */
    public function restored(Campaign $campaign)
    {
        //
    }

    /**
     * Handle the campaign "force deleted" event.
     *
     * @param  \App\Campaign  $campaign
     * @return void
     */
    public function forceDeleted(Campaign $campaign)
    {
        //
    }
}
