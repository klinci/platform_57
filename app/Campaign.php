<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use SoftDeletes;

    protected $table = 'campaigns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'project_id', 'template_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Permission.
     * @return Collection
     */
    public function permission()
    {
        return $this->hasOne(CampaignPermission::class);
    }

    /**
     * Permissions.
     * @return Collection
     */
    public function permissions()
    {
        return $this->hasMany(CampaignPermission::class);
    }

    /**
     * Project.
     * @return Collection
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Template.
     * @return Collection
     */
    public function template()
    {
        return $this->belongsTo(CampaignTemplate::class, 'template_id');
    }

    /**
     * Minisites.
     * @return Collection
     */
    public function minisites()
    {
        return $this->hasMany(Minisite::class)->orderBy('sort');
    }
}
