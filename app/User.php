<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $visible = ['id', 'name', 'email', 'roles'];

    /**
     * Workspaces
     * @return Collection
     */
    public function workspaces()
    {
        return $this->hasMany(WorkspacePermission::class);
    }

    /**
     * Teams
     * @return Collection
     */
    public function teams()
    {
        return $this->hasMany(TeamPermission::class);
    }

    /**
     * Projects
     * @return Collection
     */
    public function projects()
    {
        return $this->hasMany(ProjectPermission::class);
    }

    /**
     * Campaigns
     * @return Collection
     */
    public function campaigns()
    {
        return $this->hasMany(CampaignPermission::class);
    }
}
