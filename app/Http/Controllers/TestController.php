<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use App\Workspace;
use App\WorkspacePermission;
use App\Team;
use App\TeamPermission;
use App\Project;
use App\ProjectPermission;
use App\Campaign;
use App\CampaignPermission;
use Auth;
use App\Helpers\WorkspaceHelper;
use App\Jobs\TestJob;

class TestController extends Controller
{
    public function triggerJob()
    {
        TestJob::dispatch('Brem Solo');

        dd('DISPATCHED');
    }

    public function links()
    {
        $user = User::find(3);
        $workspacePermissions = WorkspacePermission::whereUserId($user->id)->get();
        $teamPermissions = TeamPermission::whereUserId($user->id)->get();
        $projectPermissions = ProjectPermission::whereUserId($user->id)->get();
        $campaignPermissions = CampaignPermission::whereUserId($user->id)->get();

        $workspace = collect();
        $workspacePermissions->each(function ($permission) use ($workspace) {
            $workspace->push($permission->workspace);
        });

        $hasDirectPermissionToWorkspace = false;
        if ($workspace->count() > 0) {
            $hasDirectPermissionToWorkspace = true;
        }

        $teams = collect();
        $teamPermissions->each(function ($permission) use ($teams, $workspace) {
            $teams->push($permission->team);

            /**
             * Find Workspace
             */
            if ($workspace->count() < 1) {
                $workspace->push($permission->team->workspace);
            }
        });

        $projects = collect();
        $projectPermissions->each(function ($permission) use ($projects) {
            $projects->push($permission->project);
        });

        $campaigns = collect();
        $campaignPermissions->each(function ($permission) use ($campaigns) {
            $campaigns->push($permission->campaign);
        });

        $o = new \stdClass();
        $o->workspaces = $workspace;
        $o->hasDirectPermissionToWorkspace = $hasDirectPermissionToWorkspace;
        $o->teams = $teams;
        $o->projects = $projects;
        $o->campaigns = $campaigns;

        dd($o);
    }

    public function links2($workspaceId = 1)
    {
        $workspace = Workspace::find($workspaceId);

        if (is_null($workspace)) {
            return response()->json([], Response::HTTP_OK);
        }

        $_t = [];

        if ($workspace->teams->count() > 0) {
            foreach ($workspace->teams as $team) {
                $_p = [];
                $projects = $team->projects;

                if ($projects->count() > 0) {
                    foreach ($projects as $project) {
                        if ($this->hasPermissionToProject($project->id)) {
                            $p = new \stdClass();
                            $p->name = $project->name;
                            $p->type = 'project';
                            $p->teamId = $team->id;
                            $p->teamName = $team->name;
                            $p->workspaceId = $workspaceId;
                            $p->workspaceName = $workspace->name;
                            $p->projectId = $project->id;

                            $_p[] = $p;
                        }
                    }
                } else {
                    $p = new \stdClass();
                    $p->name = '';
                    $p->type = 'project';
                    $p->teamId = '';
                    $p->teamName = '';
                    $p->workspaceId = '';
                    $p->workspaceName = '';
                    $p->projectId = '';

                    $_p[] = $p;
                }

                if ($this->hasPermissionToTeam($team->id)) {
                    $t = new \stdClass();
                    $t->name = $team->name;
                    $t->type = 'team';
                    $t->workspaceId = $workspaceId;
                    $t->workspaceName = $workspace->name;
                    $t->teamId = $team->id;
                    $t->children = $_p;

                    $_t[] = $t;
                }
            }
        } else {
            $t = new \stdClass();
            $t->name = '';
            $t->type = 'team';
            $t->workspaceId = '';
            $t->workspaceName = '';
            $t->teamId = '';
            $t->children = [];

            $_t[] = $t;
        }

        return response()->json($_t, Response::HTTP_OK);
    }

    /**
     * Check if user has permission to team
     * @param $teamId
     * @return bool
     */
    private function hasPermissionToTeam($teamId)
    {
        $teamPermission = TeamPermission::whereTeamId($teamId)->whereUserId(Auth::id())->first();

        $hasPermission = true;
        if (is_null($teamPermission)) {
            $hasPermission = false;

            $campaigns = User::find(Auth::id())->campaigns;
            if ($campaigns->count() > 0) {
                foreach ($campaigns as $campaign) {
                    if ($campaign->campaign->project->team_id == $teamId) {
                        $hasPermission = true;
                    }
                }
            }
        }

        return $hasPermission;
    }

    /**
     * Check if user has permission to project
     * @param $projectId
     * @return bool
     */
    private function hasPermissionToProject($projectId)
    {
        $projectPermission = ProjectPermission::whereProjectId($projectId)->whereUserId(Auth::id())->first();
        
        $hasPermission = true;
        if (is_null($projectPermission)) {
            $hasPermission = false;

            $campaigns = User::find(Auth::id())->campaigns;
            if ($campaigns->count() > 0) {
                foreach ($campaigns as $campaign) {
                    if ($campaign->campaign->project_id == $projectId) {
                        $hasPermission = true;
                    }
                }
            }
        }

        return $hasPermission;
    }
}
