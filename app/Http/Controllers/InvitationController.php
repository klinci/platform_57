<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\Http\Requests\Invitation\{HandleRequest, RegisterRequest};

class InvitationController extends Controller
{
    /**
     * Handle invitation.
     */
    public function handle(Invitation $invitation, HandleRequest $request)
    {
        return $request->commit($invitation);
    }

    /**
     * Handle registration.
     * @param Request
     * @return null
     */
    public function register(RegisterRequest $request)
    {
        return $request->commit();
    }
}
