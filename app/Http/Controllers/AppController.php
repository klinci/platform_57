<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AppRequest;

class AppController extends Controller
{
    /**
     * Protect controller with middleware.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Return main view to render Vue.js.
     */
    public function index(AppRequest $request)
    {
        return $request->commit();
    }
}
