<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Workspace;
use App\User;
use App\Http\Requests\API\Workspace\{ListRequest, StoreRequest};
use App\Http\Requests\API\Workspace\{UpdateRequest, ShowRequest};
use App\Http\Requests\API\Workspace\{DeleteRequest, MemberRequest};
use App\Http\Requests\API\Workspace\{TeamRequest, RevokeRequest};

class WorkspaceController extends Controller
{
    /**
     * List of Workspaces.
     * @param null
     * @return array
     */
    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    /**
     * Store Workspace.
     * @param Request
     * @return Workspace
     */
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    /**
     * Update Workspace.
     * @param Request
     * @return Workspace
     */
    public function update(Workspace $workspace, UpdateRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * Show Workspace.
     * @param Workspace
     * @return Workspace
     */
    public function show(Workspace $workspace, ShowRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * Delete Workspace.
     * @param Workspace
     * @return null
     */
    public function delete(Workspace $workspace, DeleteRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * Members of Workspace.
     * @param Workspace
     * @return Collection
     */
    public function members(Workspace $workspace, MemberRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * Teams of Workspace.
     * @param Workspace
     * @return array
     */
    public function teams(Workspace $workspace, TeamRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * Revoke user from workspace.
     * @param Workspace
     * @param User
     * @return null
     */
    public function revoke(Workspace $workspace, User $user, RevokeRequest $request)
    {
        return $request->commit($workspace, $user);
    }
}
