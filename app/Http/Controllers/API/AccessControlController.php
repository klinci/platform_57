<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\AccessControl\StatisticRequest;

class AccessControlController extends Controller
{
    /**
     * Protect controller with middleware.
     */
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    /**
     * Statistic of Access Control.
     * @return array
     */
    public function stats(StatisticRequest $request)
    {
        return $request->commit();
    }
}
