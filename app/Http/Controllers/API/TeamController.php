<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\Workspace;
use App\User;
use App\Http\Requests\API\Team\{StoreRequest, UpdateRequest};
use App\Http\Requests\API\Team\{ShowRequest, DeleteRequest};
use App\Http\Requests\API\Team\{MemberRequest, ProjectRequest};
use App\Http\Requests\API\Team\RevokeRequest;

class TeamController extends Controller
{
    /**
     * Store Team.
     * @param Request
     * @return Team
     */
    public function store(Workspace $workspace, StoreRequest $request)
    {
        return $request->commit($workspace);
    }

    /**
     * Update Team.
     * @param Request
     * @return Team
     */
    public function update(Team $team, UpdateRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * Show Team.
     * @param Team
     * @return Team
     */
    public function show(Team $team, ShowRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * Delete Team.
     * @param Team
     * @return null
     */
    public function delete(Team $team, DeleteRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * Members of Team.
     * @param Team
     * @return Collection
     */
    public function members(Team $team, MemberRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * Projects of Team.
     * @param Team
     * @return array
     */
    public function projects(Team $team, ProjectRequest $request)
    {
        return $request->commit($team);
    }

    /**
     * Revoke user from team.
     * @param Team
     * @param User
     * @return null
     */
    public function revoke(Team $team, User $user, RevokeRequest $request)
    {
        return $request->commit($team, $user);
    }
}
