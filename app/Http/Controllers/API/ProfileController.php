<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Profile\{ShowRequest, UpdateRequest};

class ProfileController extends Controller
{
    /**
     * Show Profile.
     * @return string
     */
    public function show(ShowRequest $request)
    {
        return $request->commit();
    }

    /**
     * Update profile.
     * @param Request
     * @return null
     */
    public function update(UpdateRequest $request)
    {
        return $request->commit();
    }
}
