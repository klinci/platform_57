<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\API\User\{ListRequest, StoreRequest};
use App\Http\Requests\API\User\{ShowRequest, UpdateRequest};
use App\Http\Requests\API\User\DeleteRequest;

class UserController extends Controller
{
    /**
     * Protect controller with middleware.
     */
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    /**
     * List of Users.
     * @param null
     * @return Collection
     */
    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    /**
     * Store User.
     * @param Request
     * @return User
     */
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    /**
     * Show User.
     * @param User
     * @return Collection
     */
    public function show(User $user, ShowRequest $request)
    {
        return $request->commit($user);
    }

    /**
     * Update User.
     * @param Request
     * @return User
     */
    public function update(User $user, UpdateRequest $request)
    {
        return $request->commit($user);
    }

    /**
     * Delete User.
     * @param User
     * @return null
     */
    public function delete(User $user, DeleteRequest $request)
    {
        return $request->commit($user);
    }
}
