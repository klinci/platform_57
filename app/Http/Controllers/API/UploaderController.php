<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Uploader\StoreRequest;

class UploaderController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }
}
