<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\API\AccessControl\Permission\{ListRequest, StoreRequest};
use App\Http\Requests\API\AccessControl\Permission\{ShowRequest, UpdateRequest};
use App\Http\Requests\API\AccessControl\Permission\DeleteRequest;

class PermissionController extends Controller
{
    /**
     * Protect controller with middleware.
     */
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    /**
     * List of Permissions.
     * @param null
     * @return Permission
     */
    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    /**
     * Store Permission.
     * @param Request
     * @return Permission
     */
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    /**
     * Show Permission.
     * @param Permission
     * @return Permission
     */
    public function show(Permission $permission, ShowRequest $request)
    {
        return $request->commit($permission);
    }

    /**
     * Update Permission.
     * @param Permission
     * @return Permission
     */
    public function update(Permission $permission, UpdateRequest $request)
    {
        return $request->commit($permission);
    }

    /**
     * Delete Permission.
     * @param Permission
     * @return null
     */
    public function delete(Permission $permission, DeleteRequest $request)
    {
        return $request->commit($permission);
    }
}
