<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Campaign;
use App\Project;
use App\User;
use App\Http\Requests\API\Campaign\{StoreRequest, UpdateRequest};
use App\Http\Requests\API\Campaign\{ShowRequest, DeleteRequest};
use App\Http\Requests\API\Campaign\{MemberRequest, TemplateRequest};
use App\Http\Requests\API\Campaign\RevokeRequest;

class CampaignController extends Controller
{
    /**
     * Store Campaign.
     * @param Request
     * @return Campaign
     */
    public function store(Project $project, StoreRequest $request)
    {
        return $request->commit($project);
    }

    /**
     * Update Campaign.
     * @param Request
     * @return Campaign
     */
    public function update(Campaign $campaign, UpdateRequest $request)
    {
        return $request->commit($campaign);
    }

    /**
     * Show Campaign.
     * @param Campaign
     * @return Campaign
     */
    public function show(Campaign $campaign, ShowRequest $request)
    {
        return $request->commit($campaign);
    }

    /**
     * Delete Campaign.
     * @param Campaign
     * @return null
     */
    public function delete(Campaign $campaign, DeleteRequest $request)
    {
        return $request->commit($campaign);
    }

    /**
     * Members of Campaigns.
     * @param Campaign
     * @return Collection
     */
    public function members(Campaign $campaign, MemberRequest $request)
    {
        return $request->commit($campaign);
    }

    /**
     * Campaign Templates.
     * @return array
     */
    public function templates(TemplateRequest $request)
    {
        return $request->commit();
    }

    /**
     * Revoke user from campaign.
     * @param Campaign
     * @param User
     * @return null
     */
    public function revoke(Campaign $campaign, User $user, RevokeRequest $request)
    {
        return $request->commit($campaign, $user);
    }
}
