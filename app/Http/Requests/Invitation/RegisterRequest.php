<?php

namespace App\Http\Requests\Invitation;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Helpers\GrantPermission;
use Auth;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8'
        ];
    }

    /**
     * Handle registration.
     */
    public function commit()
    {
        /**
         * Create User
         */
        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password)
        ]);

        /**
         * Grant permission depends on permission level
         */
        switch ($this->destination) {
            case 'workspace':
                GrantPermission::grantPermissionToWorkspace($this->destination_id, $user->id);
                break;
            case 'team':
                GrantPermission::grantPermissionToTeam($this->destination_id, $user->id);
                break;
            case 'project':
                GrantPermission::grantPermissionToProject($this->destination_id, $user->id);
                break;
            case 'campaign':
                GrantPermission::grantPermissionToCampaign($this->destination_id, $user->id);
                break;
        }

        /**
         * Logged in user
         */
        Auth::login($user);

        /**
         * Redirect to dashboard
         */
        return redirect('/dashboard');
    }
}
