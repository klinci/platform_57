<?php

namespace App\Http\Requests\API\Link;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Auth;
use App\{Workspace, ProjectPermission, TeamPermission, User};
use App\Hashers\{WorkspaceHasher, TeamHasher, ProjectHasher};

class LinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Return teams and projects belong to the selected workspace.
     * @return Collection
     */
    public function commit(Workspace $workspace) : Collection
    {
        $teams = collect();

        $workspace->teams->each(function ($team) use ($workspace, $teams) {
            $projects = collect();

            $team->projects->each(function ($project) use ($workspace, $team, $projects) {
                if ($this->hasPermissionToProject($project->id)) {
                    $p = new \stdClass();
                    $p->name = $project->name;
                    $p->type = 'project';
                    $p->teamId = TeamHasher::encode($team->id);
                    $p->teamName = $team->name;
                    $p->workspaceId = WorkspaceHasher::encode($workspace->id);
                    $p->workspaceName = $workspace->name;
                    $p->projectId = ProjectHasher::encode($project->id);

                    $projects->push($p);
                }
            });

            if ($team->projects->count() < 1) {
                $p = new \stdClass();
                $p->name = '';
                $p->type = 'project';
                $p->teamId = '';
                $p->teamName = '';
                $p->workspaceId = '';
                $p->workspaceName = '';
                $p->projectId = '';

                $projects->push($p);
            }

            if ($this->hasPermissionToTeam($team->id)) {
                $t = new \stdClass();
                $t->name = $team->name;
                $t->type = 'team';
                $t->workspaceId = WorkspaceHasher::encode($workspace->id);
                $t->workspaceName = $workspace->name;
                $t->teamId = TeamHasher::encode($team->id);
                $t->children = $projects;

                $teams->push($t);
            }
        });

        if ($teams->count() < 1) {
            $t = new \stdClass();
            $t->name = '';
            $t->type = 'team';
            $t->workspaceId = '';
            $t->workspaceName = '';
            $t->teamId = '';
            $t->children = [];

            $teams->push($t);
        }

        return $teams;
    }

    /**
     * Check if user has permission to team
     * @param $teamId
     * @return bool
     */
    private function hasPermissionToTeam($teamId)
    {
        /**
         * Find team permissions
         * @params $teamId
         * @return bool
         */
        $teamPermission = TeamPermission::whereTeamId($teamId)->whereUserId(Auth::id())->first();

        $hasPermission = true;
        if (is_null($teamPermission)) {
            $hasPermission = false;

            /**
             * Check if user has permissions to some campaigns
             */
            $campaigns = User::find(Auth::id())->campaigns;
            if ($campaigns->count() > 0) {
                foreach ($campaigns as $campaign) {
                    /**
                     * If the team id for campaign is same with the team id
                     * then return true
                     */
                    if ($campaign->campaign->project->team_id == $teamId) {
                        $hasPermission = true;
                    }
                }
            }
        }

        return $hasPermission;
    }

    /**
     * Check if user has permission to project
     * @param $projectId
     * @return bool
     */
    private function hasPermissionToProject($projectId)
    {
        /**
         * Find project permissions
         * @params $projectId
         * @return bool
         */
        $projectPermission = ProjectPermission::whereProjectId($projectId)->whereUserId(Auth::id())->first();
        
        $hasPermission = true;
        if (is_null($projectPermission)) {
            $hasPermission = false;

            /**
             * Check if user has permissions to some campaigns
             */
            $campaigns = User::find(Auth::id())->campaigns;
            if ($campaigns->count() > 0) {
                foreach ($campaigns as $campaign) {
                    /**
                     * If the project id for campaign is same with the project id
                     * then return true
                     */
                    if ($campaign->campaign->project_id == $projectId) {
                        $hasPermission = true;
                    }
                }
            }
        }

        return $hasPermission;
    }
}
