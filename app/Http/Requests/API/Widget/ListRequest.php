<?php

namespace App\Http\Requests\API\Widget;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\WidgetTransformer;
use App\Minisite;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * List widgets in minisite.
     * @param Minisite
     * @return array
     */
    public function commit(Minisite $minisite) : array
    {
        return fractal($minisite->widgets, WidgetTransformer::class)->toArray()['data'];
    }
}
