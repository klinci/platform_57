<?php

namespace App\Http\Requests\API\Widget;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\WidgetTransformer;
use App\Widget;

class ShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Show widget.
     * @param Widget
     * @return array
     */
    public function commit(Widget $widget)
    {
        return fractal($widget, WidgetTransformer::class)->toArray()['data'];
    }
}
