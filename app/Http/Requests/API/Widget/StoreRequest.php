<?php

namespace App\Http\Requests\API\Widget;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\WidgetTransformer;
use App\{Minisite, Widget};

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Store minisite.
     * @param Minisite
     * @return array
     */
    public function commit(Minisite $minisite) : array
    {
        $widget = new Widget();
        $widget->minisite_id = $minisite->id;
        $widget->type = $this->type;
        $widget->target = !is_null($this->target) ? $this->target : null;
        $widget->content = $this->json_content;
        $widget->save();

        return fractal($widget, WidgetTransformer::class)->toArray()['data'];
    }
}
