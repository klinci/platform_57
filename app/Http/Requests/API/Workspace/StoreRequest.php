<?php

namespace App\Http\Requests\API\Workspace;

use Illuminate\Foundation\Http\FormRequest;
use App\{Workspace, WorkspacePermission};
use App\Transformers\WorkspaceTransformer;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    /**
     * Store Workspace.
     * @return array
     */
    public function commit() : array
    {
        $workspace = Workspace::create(['name' => $this->name]);

        WorkspacePermission::create([
            'workspace_id' => $workspace->id,
            'user_id' => Auth::id(),
            'role_id' => env('OWNER_ROLE_ID')
        ]);

        return fractal($workspace, WorkspaceTransformer::class)->toArray()['data'];
    }
}
