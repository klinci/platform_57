<?php

namespace App\Http\Requests\API\Workspace;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\WorkspaceHelper;
use App\Transformers\WorkspaceTransformer;
use Auth;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * List of Workspaces.
     * @return array
     */
    public function commit() : array
    {
        return fractal(WorkspaceHelper::workspaces(Auth::id()), WorkspaceTransformer::class)->toArray()['data'];
    }
}
