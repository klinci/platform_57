<?php

namespace App\Http\Requests\API\Workspace;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\RevokePermission;
use App\Workspace;
use App\User;

class RevokeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Revoke user from workspace.
     * @param Workspace
     * @param User
     * @return null
     */
    public function commit(Workspace $workspace, User $user)
    {
        return RevokePermission::revokePermissionFromWorkspace($workspace->id, $user->id);
    }
}
