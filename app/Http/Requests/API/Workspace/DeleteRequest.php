<?php

namespace App\Http\Requests\API\Workspace;

use Illuminate\Foundation\Http\FormRequest;
use App\Workspace;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Delete Workspace.
     * @return null
     */
    public function commit(Workspace $workspace)
    {
        /**
         * Find teams belongs to workspace
         */
        $workspace->teams->each(function ($team) {
            /**
             * Find projects belong to team
             */
            $team->projects->each(function ($project) {
                /**
                 * Find campaigns belong to project
                 */
                $project->campaigns->each(function ($campaign) {
                    /**
                     * Delete campaign permissions
                     */
                    $campaign->permissions->each->delete();
                });

                /**
                 * Delete campaigns belongs to project
                 */
                $project->campaigns->each->delete();

                /**
                 * Delete project permissions
                 */
                $project->permissions->each->delete();
            });

            /**
             * Delete projects belongs to team
             */
            $team->projects->each->delete();

            /**
             * Delete team permissions
             */
            $team->permissions->each->delete();
        });

        /**
         * Delete teams belongs to workspace
         */
        $workspace->teams->each->delete();

        /**
         * Delete workspace permissions
         */
        $workspace->permissions->each->delete();

        /**
         * Delete workspace
         */
        $workspace->delete();

        return null;
    }
}
