<?php

namespace App\Http\Requests\API\Workspace;

use Illuminate\Foundation\Http\FormRequest;
use App\Workspace;
use App\Transformers\WorkspaceTransformer;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    /**
     * Update Workspace.
     * @return array
     */
    public function commit(Workspace $workspace) : array
    {
        $workspace->name = $this->name;
        $workspace->update();

        return fractal($workspace, WorkspaceTransformer::class)->toArray()['data'];
    }
}
