<?php

namespace App\Http\Requests\API\Utility;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Hashers\WorkspaceHasher;

class GetFirstWorkspace extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function commit() : string
    {
        $encoded = '';

        if (Auth::check()) {
            $workspaces = Auth::user()->workspaces;

            if ($workspaces->count() > 0) {
                $encoded = WorkspaceHasher::encode($workspaces->first()->workspace->id);
            }
        }

        return $encoded;
    }
}
