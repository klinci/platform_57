<?php

namespace App\Http\Requests\API\Utility;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Hashers\ProjectHasher;
use App\Team;

class GetFirstProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get first project unique id for logged in user
     * @param null
     * @return string
     */
    public function commit(Team $team) : string
    {
        $encoded = '';

        if (Auth::check()) {
            $projects = collect();
            
            Auth::user()->projects->each(function ($permission) use ($team, $projects) {
                if ($permission->project->team_id == $team->id) {
                    $projects->push($permission->project);
                }
            });

            if ($projects->count() > 0) {
                $encoded = ProjectHasher::encode($projects->first()->id);
            }
        }

        return $encoded;
    }
}
