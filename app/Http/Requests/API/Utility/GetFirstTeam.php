<?php

namespace App\Http\Requests\API\Utility;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Hashers\TeamHasher;
use App\Workspace;

class GetFirstTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get first team unique id for logged in user
     * @param Workspace
     * @return string
     */
    public function commit(Workspace $workspace) : string
    {
        $encoded = '';

        if (Auth::check()) {
            $teams = collect();

            Auth::user()->teams->each(function ($permission) use ($workspace, $teams) {
                if ($permission->team->workspace_id == $workspace->id) {
                    $teams->push($permission->team);
                }
            });

            if ($teams->count() > 0) {
                $encoded = TeamHasher::encode($teams->first()->id);
            }
        }

        return $encoded;
    }
}
