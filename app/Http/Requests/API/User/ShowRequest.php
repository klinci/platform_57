<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use App\User;

class ShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Show User.
     * @return Collection
     */
    public function commit(User $user): Collection
    {
        $colletion = collect($user);
        $colletion->put('roles', $user->roles->pluck('name')->all());

        return $colletion;
    }
}
