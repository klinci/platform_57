<?php

namespace App\Http\Requests\API\Team;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\RevokePermission;
use App\Team;
use App\User;

class RevokeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Revoke user from team.
     * @param Team
     * @param User
     * @return null
     */
    public function commit(Team $team, User $user)
    {
        return RevokePermission::revokePermissionFromTeam($team->id, $user->id);
    }
}
