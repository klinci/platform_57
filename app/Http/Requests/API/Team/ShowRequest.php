<?php

namespace App\Http\Requests\API\Team;

use Illuminate\Foundation\Http\FormRequest;
use App\Team;
use App\Transformers\TeamTransformer;

class ShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Show Team.
     * @return array
     */
    public function commit(Team $team) : array
    {
        return fractal($team, TeamTransformer::class)->toArray()['data'];
    }
}
