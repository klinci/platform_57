<?php

namespace App\Http\Requests\API\Team;

use Illuminate\Foundation\Http\FormRequest;
use App\Team;
use App\Transformers\ProjectTransformer;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Projects of Team.
     * @return array
     */
    public function commit(Team $team) : array
    {
        return fractal($team->projects, ProjectTransformer::class)->toArray()['data'];
    }
}
