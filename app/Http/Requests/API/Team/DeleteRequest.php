<?php

namespace App\Http\Requests\API\Team;

use Illuminate\Foundation\Http\FormRequest;
use App\Team;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Delete Team.
     * @return null
     */
    public function commit(Team $team)
    {
        /**
         * Find projects belongs to project
         */
        $team->projects->each(function ($project) {
            /**
             * Find campaigns belong to project
             */
            $project->campaigns->each(function ($campaign) {
                /**
                 * Delete campaign permissions
                 */
                $campaign->permissions->each->delete();
            });


            /**
             * Delete campaigns belongs to project
             */
            $project->campaigns->each->delete();

            /**
             * Delete project permission
             */
            $project->permissions->each->delete();
        });

        /**
         * Delete projects belongs to project
         */
        $team->projects->each->delete();

        /**
         * Delete team permissions
         */
        $team->permissions->each->delete();

        /**
         * Delete team
         */
        $team->delete();

        return null;
    }
}
