<?php

namespace App\Http\Requests\API\Team;

use Illuminate\Foundation\Http\FormRequest;
use App\{Team, TeamPermission, Workspace, WorkspacePermission};
use App\Helpers\GrantPermission;
use App\Transformers\TeamTransformer;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    /**
     * Store Team.
     * @return array
     */
    public function commit(Workspace $workspace) : array
    {
        $team = Team::create(['name' => $this->name, 'workspace_id' => $workspace->id]);

        TeamPermission::create([
            'team_id' => $team->id,
            'user_id' => Auth::id(),
            'role_id' => env('OWNER_ROLE_ID')
        ]);

        /**
         * Find users except owner in the same workspace
         */
        $users = WorkspacePermission::whereWorkspaceId($workspace->id)->whereNotIn('user_id', [Auth::id()])->get();
        $users->each(function ($permission) use ($team) {
            /**
             * Grant permission to team for each user found
             */
            GrantPermission::assignPermissionToTeam($team->id, $permission->user_id);
        });

        return fractal($team, TeamTransformer::class)->toArray()['data'];
    }
}
