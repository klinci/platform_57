<?php

namespace App\Http\Requests\API\AccessControl\Role;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'guard_name' => 'required'
        ];
    }

    /**
     * Store Role.
     * @return Role
     */
    public function commit() : Role
    {
        $role = Role::create(['name' => $this->name, 'guard_name' => $this->guard_name]);
        $role->syncPermissions($this->permissions);

        return $role;
    }
}
