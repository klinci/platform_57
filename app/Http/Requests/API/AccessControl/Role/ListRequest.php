<?php

namespace App\Http\Requests\API\AccessControl\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Permission\Models\Role;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * List of Roles.
     * @return Collection
     */
    public function commit() : Collection
    {
        return Role::all();
    }
}
