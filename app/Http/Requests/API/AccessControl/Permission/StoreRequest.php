<?php

namespace App\Http\Requests\API\AccessControl\Permission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Permission\Models\Permission;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'guard_name' => 'required'
        ];
    }

    /**
     * Store Permission.
     * @return Permission
     */
    public function commit() : Permission
    {
        return Permission::create([
            'name' => $this->name,
            'guard_name' => $this->guard_name
        ]);
    }
}
