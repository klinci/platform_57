<?php

namespace App\Http\Requests\API\AccessControl;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\{Role, Permission};

class StatisticRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Statistic of Access Control.
     * @return Collection
     */
    public function commit() : Collection
    {
        $role = new \stdClass();
        $role->type = 'warning';
        $role->icon = 'ti-key';
        $role->title = 'Roles';
        $role->value = Role::all()->count();

        $permission = new \stdClass();
        $permission->type = 'warning';
        $permission->icon = 'ti-key';
        $permission->title = 'Permissions';
        $permission->value = Permission::all()->count();

        $statistics = collect();
        $statistics->push($role);
        $statistics->push($permission);

        return $statistics;
    }
}
