<?php

namespace App\Http\Requests\API\Project;

use Illuminate\Foundation\Http\FormRequest;
use App\Project;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Delete Project.
     * @return null
     */
    public function commit(Project $project)
    {
        /**
         * Find campaigns belongs to project
         */
        $project->campaigns->each(function ($campaign) {
            /**
             * Delete campaign permissions
             */
            $campaign->permissions->each->delete();
        });

        /**
         * Delete campaigns belongs to project
         */
        $project->campaigns->each->delete();

        /**
         * Delete project permissions
         */
        $project->permissions->each->delete();

        /**
         * Delete project
         */
        $project->delete();

        return null;
    }
}
