<?php

namespace App\Http\Requests\API\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use App\{Project, CampaignPermission};
use App\Hashers\CampaignHasher;
use App\Helpers\{TimeHelper, MemberHelper};
use Auth;

class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Campaigns of Project.
     * @return array
     */
    public function commit(Project $project) : array
    {
        $campaigns = collect();

        $project->campaigns->each(function ($c) use ($campaigns) {
            if ($this->hasPermissionToCampaign($c->id)) {
                $campaign = new \stdClass();
                $campaign->id = CampaignHasher::encode($c->id);
                $campaign->name = $c->name;
                $campaign->type = $c->template->name;
                $campaign->status = is_null($c->status) ? 'Draft' : $c->status;
                $campaign->created_at = date('d M Y', strtotime($c->created_at));
                $campaign->updated_at_relative = TimeHelper::findTimeAgo(date('Y-m-d H:i:s', strtotime($c->updated_at)));
                $campaign->updated_at = $c->updated_at;
                $campaign->members = MemberHelper::campaign($c);

                $campaigns->push($campaign);
            }
        });

        $sorted = $campaigns->sortByDesc('updated_at');

        return $sorted->values()->all();
    }

    /**
     * Check if user has permission to campaign
     * @return bool
     */
    private function hasPermissionToCampaign($campaignId) : bool
    {
        $campaignPermission = CampaignPermission::whereCampaignId($campaignId)->whereUserId(Auth::id())->first();

        $hasPermission = false;
        if (!is_null($campaignPermission)) {
            $hasPermission = true;
        }

        return $hasPermission;
    }
}
