<?php

namespace App\Http\Requests\API\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use App\Project;
use App\Helpers\MemberHelper;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Members of Project.
     * @return Collection
     */
    public function commit(Project $project) : Collection
    {
        return MemberHelper::project($project);
    }
}
