<?php

namespace App\Http\Requests\API\Project;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\RevokePermission;
use App\Project;
use App\User;

class RevokeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Revoke user from project.
     * @param Project
     * @param User
     * @return null
     */
    public function commit(Project $project, User $user)
    {
        return RevokePermission::revokePermissionFromProject($project->id, $user->id);
    }
}
