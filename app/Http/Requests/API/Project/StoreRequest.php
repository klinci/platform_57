<?php

namespace App\Http\Requests\API\Project;

use Illuminate\Foundation\Http\FormRequest;
use App\{Project, ProjectPermission, Team, TeamPermission};
use App\Helpers\GrantPermission;
use App\Transformers\ProjectTransformer;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    /**
     * Store Project.
     * @return array
     */
    public function commit(Team $team) : array
    {
        $project = Project::create(['name' => $this->name, 'team_id' => $team->id]);

        ProjectPermission::create([
            'project_id' => $project->id,
            'user_id' => Auth::id(),
            'role_id' => env('OWNER_ROLE_ID')
        ]);

        /**
         * Find users except owner in the same team
         */
        $users = TeamPermission::whereTeamId($team->id)->whereNotIn('user_id', [Auth::id()])->get();
        $users->each(function ($permission) use ($project) {
            /**
             * Grant permission to project for each user found
             */
            GrantPermission::assignPermissionToProject($project->id, $permission->user_id);
        });

        return fractal($project, ProjectTransformer::class)->toArray()['data'];
    }
}
