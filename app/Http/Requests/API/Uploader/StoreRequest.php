<?php

namespace App\Http\Requests\API\Uploader;

use Illuminate\Foundation\Http\FormRequest;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Handle upload
     */
    public function commit()
    {
        Storage::disk('s3')->put('images', $this->file);
    }
}
