<?php

namespace App\Http\Requests\API\Invitation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use App\{Project, User, Invitation};
use App\Helpers\GrantPermission;
use App\Jobs\ProcessProjectInvitation;
use Auth;
use App\Hashers\UserHasher;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Send invitation to project.
     * @return Collection
     */
    public function commit(Project $project) : Collection
    {
        $members = collect();

        $emails = collect(explode(',', $this->emails));
        $emails->each(function ($email) use ($members, $project) {
            $email = trim(strtolower($email));

            if (!empty($email) && ($email !== Auth::user()->email)) {
                /**
                 * Find User
                 */
                $user = User::whereEmail($email)->first();

                /**
                 * Add to invitation
                 */
                $invitation = Invitation::create([
                    'email' => $email,
                    'destination' => 'project',
                    'destination_id' => $project->id
                ]);

                $isRegisteredUser = false;
                $name = $email;
                $memberUserID = null;

                /**
                 * Assign permission
                 */
                if (!is_null($user)) {
                    $isRegisteredUser = true;
                    $name = $user->name;

                    /**
                     * Grant permission to workspace
                     */
                    GrantPermission::grantPermissionToProject($project->id, $user->id);

                    $memberUserID = UserHasher::encode($user->id);
                }

                /**
                 * Dispatch Job
                 */
                ProcessProjectInvitation::dispatch($invitation, $project, $isRegisteredUser);

                $member = new \stdClass();
                $member->id = $memberUserID;
                $member->name = $name;
                $member->email = $email;
                $member->role = 'member';

                $members->push($member);
            }
        });

        return $members;
    }
}
