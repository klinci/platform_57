<?php

namespace App\Http\Requests\API\Minisite;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\MinisiteTransformer;
use App\Minisite;
use App\Hashers\MinisiteHasher;
use Storage;

class UpdateMinisiteTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Update minisite template.
     * @return array
     */
    public function commit(Minisite $minisite) : array
    {
        $template = json_decode(Storage::get(sprintf('json/minisites/%s.json', $this->template)));

        $minisite->type = $this->template;
        $minisite->styles = json_encode($template->styles);
        $minisite->header = json_encode($template->header);
        $minisite->content = json_encode($template->content);
        $minisite->footer = json_encode($template->footer);

        $minisite->update();

        return fractal($minisite, MinisiteTransformer::class)->toArray()['data'];
    }
}
