<?php

namespace App\Http\Requests\API\Profile;

use Illuminate\Foundation\Http\FormRequest;
use App\Transformers\ProfileTransformer;
use Auth;

class ShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Show Profile.
     * @return array
     */
    public function commit() : array
    {
        return fractal(Auth::user(), ProfileTransformer::class)->toArray()['data'];
    }
}
