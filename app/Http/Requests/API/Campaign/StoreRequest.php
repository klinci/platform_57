<?php

namespace App\Http\Requests\API\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use App\{Campaign, CampaignPermission};
use App\{Project, ProjectPermission};
use App\{TeamPermission, WorkspacePermission};
use App\Helpers\GrantPermission;
use App\Transformers\CampaignTransformer;
use Auth;
use App\Hashers\CampaignTemplateHasher;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'template_id' => 'required'
        ];
    }

    /**
     * Store Campaign.
     * @return array
     */
    public function commit(Project $project) : array
    {
        $campaign = Campaign::create([
            'name' => $this->name,
            'project_id' => $project->id,
            'template_id' => CampaignTemplateHasher::decode($this->template_id)
        ]);

        CampaignPermission::create([
            'campaign_id' => $campaign->id,
            'user_id' => Auth::id(),
            'role_id' => env('OWNER_ROLE_ID')
        ]);

        /**
         * Find users except owner in the same project
         */
        $userProjects = ProjectPermission::whereProjectId($project->id)->whereNotIn('user_id', [Auth::id()])->get();
        $userProjects->each(function ($projectPermission) use ($campaign) {
            /**
             * Grant permission to campaign for each user found
             */
            GrantPermission::assignPermissionToCampaign($campaign->id, $projectPermission->user_id);

            /**
             * Find users except owner in the same team
             */
            $userTeams = TeamPermission::whereTeamId($projectPermission->project->team_id)->whereNotIn('user_id', [Auth::id()])->get();
            $userTeams->each(function ($teamPermission) use ($campaign) {
                /**
                 * Grant permission to campaign for each user found
                 */
                GrantPermission::assignPermissionToCampaign($campaign->id, $teamPermission->user_id);

                /**
                 * Find users except owner in the same workspace
                 */
                $userWorkspaces = WorkspacePermission::whereWorkspaceId($teamPermission->team->workspace_id)->whereNotIn('user_id', [Auth::id()])->get();
                $userWorkspaces->each(function ($workspacePermission) use ($campaign) {
                    /**
                     * Grant permission to campaign for each user found
                     */
                    GrantPermission::assignPermissionToCampaign($campaign->id, $workspacePermission->user_id);
                });
            });
        });

        return fractal($campaign, CampaignTransformer::class)->toArray()['data'];
    }
}
