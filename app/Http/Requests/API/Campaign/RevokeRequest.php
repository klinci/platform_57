<?php

namespace App\Http\Requests\API\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\RevokePermission;
use App\Campaign;
use App\User;

class RevokeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Revoke user from campaign.
     * @param Campaign
     * @param User
     * @return null
     */
    public function commit(Campaign $campaign, User $user)
    {
        return RevokePermission::revokePermissionFromCampaign($campaign->id, $user->id);
    }
}
