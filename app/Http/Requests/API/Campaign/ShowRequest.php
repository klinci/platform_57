<?php

namespace App\Http\Requests\API\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use App\Campaign;
use App\Transformers\CampaignTransformer;

class ShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Show Campaign.
     * @return array
     */
    public function commit(Campaign $campaign) : array
    {
        return fractal($campaign, CampaignTransformer::class)->toArray()['data'];
    }
}
