<?php

namespace App\Http\Requests\API\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use App\Campaign;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Delete Campaign.
     * @return null
     */
    public function commit(Campaign $campaign)
    {
        /**
         * Delete campaign permissions
         */
        $campaign->permissions->each->delete();

        /**
         * Delete campaign
         */
        $campaign->delete();

        return null;
    }
}
