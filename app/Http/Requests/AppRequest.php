<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JavaScript;
use Auth;
use App\Hashers\WorkspaceHasher;

class AppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * App Request.
     */
    public function commit()
    {
        JavaScript::put([
            'user' => Auth::user()->toJson(),
            'isAdmin' => Auth::user()->hasRole('admin'),
            'logoDirectory' => env('IMAGE_ASSET_DIRECTORY'),
            'appName' => env('APP_NAME'),
            'scssDirectory' => env('SCSS_ASSET_DIRECTORY'),
            'isFirstTimeLoggedIn' => is_null(Auth::user()->last_logged_in) ? true : false,
            'workspace' => $this->getFirstWorkspace()
        ]);

        $user = Auth::user();
        $user->last_logged_in = date('Y-m-d H:i:s');
        $user->update();

        return view('index');
    }

    /**
     * Get first workspace if any
     */
    public function getFirstWorkspace()
    {
        $workspaces = Auth::user()->workspaces;

        $encoded = '';

        if ($workspaces->count() > 0) {
            $encoded = WorkspaceHasher::encode($workspaces->first()->workspace->id);
        }

        return $encoded;
    }
}
