<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Invitation extends Model
{
    use Notifiable;

    protected $table = 'invitations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'destination', 'destination_id'];

    /**
     * Workspace
     * @return Collection
     */
    public function workspace()
    {
        return $this->belongsTo(Workspace::class, 'destination_id');
    }

    /**
     * Team
     * @return Collection
     */
    public function team()
    {
        return $this->belongsTo(Team::class, 'destination_id');
    }

    /**
     * Project
     * @return Collection
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'destination_id');
    }

    /**
     * Campaign
     * @return Collection
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'destination_id');
    }
}
