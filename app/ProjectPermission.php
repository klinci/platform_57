<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPermission extends Model
{
    protected $table = 'project_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'user_id', 'role_id'
    ];

    /**
     * Project
     * @return Collection
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * User
     * @return Collection
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Role
     * @return Collection
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
