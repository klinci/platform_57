<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Hashers\ProjectHasher;
use App\Hashers\InvitationHasher;

class ProjectInvitation extends Notification
{
    use Queueable;

    protected $project;
    protected $invitation;
    protected $isRegisteredUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($project, $invitation, $isRegisteredUser)
    {
        $this->project = $project;
        $this->invitation = $invitation;
        $this->isRegisteredUser = $isRegisteredUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->isRegisteredUser) {
            return $this->toRegisteredUser();
        } else {
            return $this->toUnregisteredUser();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Send invitation email to registered user
     */
    public function toRegisteredUser()
    {
        $url = url(sprintf('/project/%s', ProjectHasher::encode($this->project->id)));

        return (new MailMessage)
                    ->subject(sprintf(
                        '%s added you to "%s"',
                        $this->project->permission->user->name,
                        $this->project->name
                    ))
                    ->line(sprintf(
                        '%s added you to %s project',
                        $this->project->permission->user->name,
                        $this->project->name
                    ))
                    ->line('Welcome to the project')
                    ->action('View Project', $url);
    }

    /**
     * Send invitation email to unregistered user
     */
    public function toUnregisteredUser()
    {
        $url = url(sprintf('/invitation/%s', InvitationHasher::encode($this->invitation->id)));
        
        return (new MailMessage)
                    ->subject(sprintf(
                        '%s invited you to join "%s"',
                        $this->project->permission->user->name,
                        $this->project->name
                    ))
                    ->line(sprintf(
                        '%s invited you to join %s',
                        $this->project->permission->user->name,
                        $this->project->name
                    ))
                    ->action('Accept Invite', $url);
    }
}
