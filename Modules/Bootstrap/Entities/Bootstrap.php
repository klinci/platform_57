<?php

namespace Modules\Bootstrap\Entities;

use Illuminate\Database\Eloquent\Model;

class Bootstrap extends Model
{
    protected $table = 'bootstrap_dummy_data';
    protected $fillable = [
        'name',
        'text'
    ];
}
