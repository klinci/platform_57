const { mix } = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

/*mix.webpackConfig({
  resolve: {
    modules: [
      path.resolve(__dirname, 'resources/assets/js')
    ]
  }
});*/

mix.js(__dirname + '/Resources/assets/js/app.js', 'js/bootstrap.js')
  .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/bootstrap.css');

if (mix.inProduction()) {
  mix.version();
}
