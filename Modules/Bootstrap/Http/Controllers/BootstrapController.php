<?php

namespace Modules\Bootstrap\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Bootstrap\Entities\Bootstrap;

class BootstrapController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('bootstrap::index');
    }

    public function list()
    {
        return response()->json(Bootstrap::all(), Response::HTTP_OK);
    }

    public function show($id)
    {
        return response()->json(Bootstrap::findOrFail($id), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $dummyData = Bootstrap::create(['name' => $request->name, 'text' => $request->text]);

        return response()->json($dummyData, Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $dummyData = Bootstrap::findOrFail($id);
        $dummyData->update($request->all());

        return response()->json($dummyData, Response::HTTP_OK);
    }

    public function delete($id)
    {
        $dummyData = Bootstrap::findOrFail($id);
        $dummyData->delete();

        return null;
    }
}
