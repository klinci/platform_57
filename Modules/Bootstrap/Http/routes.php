<?php

Route::group(['middleware' => 'web', 'prefix' => 'bootstrap', 'namespace' => 'Modules\Bootstrap\Http\Controllers'], function()
{
    Route::get('/', 'BootstrapController@index');

    Route::get('/dummy-data', 'BootstrapController@list');
    Route::get('/dummy-data/{id}', 'BootstrapController@show');
    Route::post('/dummy-data', 'BootstrapController@store');
    Route::put('/dummy-data/{id}', 'BootstrapController@update');
    Route::delete('/dummy-data/{id}', 'BootstrapController@delete');
});
