import DashboardLayout from './../../../../../../resources/assets/js/components/Dashboard/Layout/DashboardLayout.vue'
import BootstrapOverview from './../components/BootstrapOverview.vue'
import NotFound from './../../../../../../resources/assets/js/components/GeneralViews/NotFoundPage.vue'

/**
 * Bootstrap Form Demo
 */
import BootstrapDummyDataList from './../components/BootstrapDummyDataList.vue'
import BootstrapDummyDataCreate from './../components/BootstrapDummyDataCreate.vue'
import BootstrapDummyDataEdit from './../components/BootstrapDummyDataEdit.vue'

let formDemoPage = {
  path: '/bootstrap/dummy',
  component: DashboardLayout,
  children: [
    {
      path: '/bootstrap/dummy',
      name: 'Dummy Data List',
      component: BootstrapDummyDataList
    },
    {
      path: '/bootstrap/dummy/create',
      name: 'Create Dummy Data',
      component: BootstrapDummyDataCreate
    },
    {
      path: '/bootstrap/dummy/edit/:id',
      name: 'Edit Dummy Data',
      component: BootstrapDummyDataEdit
    }
  ]
}

const routes = [
  formDemoPage,
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/bootstrap/overview',
    children: [
      {
        path: '/bootstrap/overview',
        name: 'Bootstrap Overview',
        component: BootstrapOverview
      },
    ]
  },
  {
    path: '*',
    component: NotFound
  }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
