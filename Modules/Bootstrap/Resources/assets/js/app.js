import Vue from 'vue'
import './../../../../../resources/assets/js/pollyfills'
import VueRouter from 'vue-router'
import VueNotify from 'vue-notifyjs'
import VeeValidate from 'vee-validate'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import App from './../../../../../resources/assets/js/App.vue'

// Plugins
import GlobalComponents from './../../../../../resources/assets/js/globalComponents'
import GlobalDirectives from './../../../../../resources/assets/js/globalDirectives'
import SideBar from './../../../../../resources/assets/js/components/UIComponents/SidebarPlugin'

// Router
import routes from './routes/routes'

// Library Imports
import './../../../../../resources/assets/js/assets/sass/paper-dashboard.scss'
import './../../../../../resources/assets/js/assets/sass/element_variables.scss'
import './../../../../../resources/assets/js/assets/sass/demo.scss'

import sidebarLinks from './sidebarLinks'

// Plugin Setup
Vue.use(VueRouter)
Vue.use(GlobalDirectives)
Vue.use(GlobalComponents)
Vue.use(VueNotify)
Vue.use(SideBar, {sidebarLinks: sidebarLinks})
Vue.use(VeeValidate)
locale.use(lang)

// Configure Router
const router = new VueRouter({
  routes,
  linkActiveClass: 'active'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router
})
