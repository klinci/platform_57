<?php

namespace Modules\Integration\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Integration\Entities\Integration;
use Auth;
use JavaScript;

class IntegrationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        JavaScript::put([
            'user' => Auth::user()->toJson(),
            'isAdmin' => Auth::user()->hasRole('admin'),
            'logoDirectory' => env('IMAGE_ASSET_DIRECTORY'),
            'appName' => env('APP_NAME'),
            'scssDirectory' => env('SCSS_ASSET_DIRECTORY')
        ]);

        return view('integration::index');
    }

    public function store(Request $request)
    {
        $integration = Integration::create([
            'name' => $request->name,
            'service' => $request->service,
            'config' => json_encode($request->config),
            'workspace_id' => 1,
            'user_id' => Auth::id()
        ]);

        return response()->json($integration, Response::HTTP_OK);
    }

    public function integrations()
    {
        return response()->json(Integration::all(), Response::HTTP_OK);
    }
}
