<?php

/**
 * Web Routes
 */
Route::group([
    'middleware' => 'web',
    'namespace' => 'Modules\Integration\Http\Controllers'
],
function () {
    Route::get('integration', 'IntegrationController@index');
});

/**
 * API Routes
 */
Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'Modules\Integration\Http\Controllers',
    'prefix' => 'api'
],
function () {
    Route::get('integrations', 'IntegrationController@integrations');
    Route::post('integration', 'IntegrationController@store');
});
