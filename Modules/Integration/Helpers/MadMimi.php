<?php

namespace Modules\Integration\Helpers;

use Modules\MadMimi\MailProviderMadMimi;

class MadMimi
{
    protected $service;

    public function __construct($email, $apiKey)
    {
        $this->service = new MailProviderMadMimi($email, $apiKey);
    }

    public function lists()
    {
        $madmimiLists = [];
        try {
            $_MadL = $this->service->lists();
            if (!empty($_MadL)) {
                foreach ($_MadL as $list) {
                    $madmimiLists[$list->id] = $list->name;
                }
            }      
        } catch (\Exception $e) {
            return $e;
        }
        
        return $madmimiLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $name = [
                'first_name' => $request->firstName,
                'last_name' => $request->lastName
            ];
            $subscribe = $this->service->subscribe($list, $request->email, $name);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $perPage, $page)
    {
        try {
            $contacts = $this->service->contacts($list, $perPage, $page);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
