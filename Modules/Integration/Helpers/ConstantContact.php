<?php

namespace Modules\Integration\Helpers;

use Ctct\ConstantContact as ConstantContactLib;
use Ctct\Exceptions\CtctException;
use Ctct\Components\Contacts\Contact;

class ConstantContact
{
    protected $service;
    protected $accessToken;

    public function __construct($accessToken)
    {
        $this->service = new ConstantContactLib(env('CONSTANTCONTACT_APIKEY'));
        $this->accessToken = $accessToken;
    }

    public function lists()
    {
        $constantcontactLists = [];
        try {
            $aLists = $this->service->listService->getLists($this->accessToken);
            $lists = (object) $aLists;
            foreach ($lists as $list) {
                $constantcontactLists[$list->id] = $list->name;
            }
        } catch (CtctException $e) {
            return $e;
        }
        
        return $constantcontactLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $contact = new Contact();
            $contact->addEmail($request->email);
            $contact->addList($list);
            $contact->first_name = !is_null($request->firstName) ? $request->firstName : '';
            $contact->last_name = !is_null($request->lastName) ? $request->lastName : '';

            $subscribe = $this->service->contactService->addContact(
                $this->accessToken,
                $contact,
                'ACTION_BY_VISITOR'
            );
        } catch (CtctException $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $params = [])
    {
        try {
            $contacts = $this->service->contactService->getContactsFromList($this->accessToken, $list, $params);
        } catch (CtctException $e) {
            return $e;
        }

        return $contacts;
    }
}
