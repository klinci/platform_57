<?php

namespace Modules\Integration\Helpers;

use Modules\GetResponse\MailProviderGetResponse;

class GetResponse
{
    protected $service;

    public function __construct($apiKey)
    {
        $this->service = new MailProviderGetResponse($apiKey);
    }

    public function lists()
    {
        $getresponseLists = [];
        try {
            $_GRL = $this->service->lists();
            if (!empty($_GRL)) {
                foreach ($_GRL as $list) {
                    $getresponseLists[$list->id] = $list->name;
                }
            }      
        } catch (\Exception $e) {
            return $e;
        }

        return $getresponseLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $name = $request->firstName . ' ' . $request->lastName;
            $subscribe = $this->service->subscribe($list, $request->email, $name);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $perPage, $page)
    {
        try {
            $contacts = $this->service->contacts($list, $perPage, $page);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
