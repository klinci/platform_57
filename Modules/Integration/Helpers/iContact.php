<?php

namespace Modules\Integration\Helpers;

use Modules\IContact\MailProvideriContact;

class iContact
{
    protected $service;

    public function __construct($appID, $password, $username)
    {
        $this->service = new MailProvideriContact($appID, $password, $username);
    }

    public function lists()
    {
        $icontactLists = [];
        try {
            $_iCoL = $this->service->lists();
            if (!empty($_iCoL)) {
                foreach ($_iCoL as $list) {
                    $icontactLists[$list->id] = $list->name;
                }
            }
        } catch (\Exception $e) {
            return $e;
        }
        
        return $icontactLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $subscribe = $this->service->subscribe($list, $request->email, $request->firstName, $request->lastName);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $offset, $limit)
    {
        try {
            $contacts = $this->service->contacts($list, $offset, $limit);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
