<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactActiveCampaign;

class ActiveCampaign extends Queue
{
    public function import($list, $apiURL, $apiKey)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'activecampaign';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactActiveCampaign::dispatch(
            $list,
            100,
            1,
            $apiURL,
            $apiKey,
            $importJob->id
        );

        return $importJob;
    }
}
