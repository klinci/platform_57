<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactAweber;

class Aweber extends Queue
{
    public function import($list, $token)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'aweber';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactAweber::dispatch(
            $list,
            0,
            100,
            $token,
            $importJob->id
        );

        return $importJob;
    }
}
