<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactConstantContact;

class ConstantContact extends Queue
{
    public function import($list, $token)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'constantcontact';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactConstantContact::dispatch(
            $list,
            100,
            $token,
            $importJob->id
        );

        return $importJob;
    }
}
