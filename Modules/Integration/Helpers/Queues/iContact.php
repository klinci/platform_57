<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactiContact;

class iContact extends Queue
{
    public function import($list, $appID, $appPassword, $appUsername)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'icontact';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactiContact::dispatch(
            $list,
            0,
            100,
            $appID,
            $appPassword,
            $appUsername,
            $importJob->id
        );

        return $importJob;
    }
}
