<?php

namespace Modules\Integration\Helpers\Queues;

use Auth;
use Modules\Integration\Helpers\Queues\Queue;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Jobs\ProcessImportContactMailChimp;

class MailChimp extends Queue
{
    public function import($list, $apiKey)
    {
        $importJob = new ImportJob();
        $importJob->user_id = Auth::id();
        $importJob->list_id = $list;
        $importJob->provider = 'mailchimp';
        $importJob->status = 'ONPROGRESS';
        $importJob->save();

        ProcessImportContactMailChimp::dispatch(
            $list,
            0,
            100,
            $apiKey,
            $importJob->id
        );

        return $importJob;
    }
}
