<?php

namespace Modules\Integration\Helpers;

use Illuminate\Http\Request;
use Modules\Aweber\MailProviderAweber;

class Aweber
{
    protected $service;

    public function __construct($credential)
    {
        $aweberCred = json_decode($credential);
        $this->service = new MailProviderAweber(
            $aweberCred->consumerKey,
            $aweberCred->consumerSecret,
            $aweberCred->accessKey,
            $aweberCred->accessSecret
        );
    }

    public function lists()
    {
        $aweberLists = [];
        try {
            $_AL = $this->service->lists();
            if (!empty($_AL)) {
                foreach ($_AL as $list) {
                    $aweberLists[$list->id] = $list->name;
                }
            }
        } catch (\Exception $e) {
            return $e;
        }

        return $aweberLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $name = $request->firstName . ' ' . $request->lastName;
            $subscribe = $this->service->subscribe($list, $request->email, $name);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $perPage, $page)
    {
        try {
            $contacts = $this->service->contacts($list, $perPage, $page);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
