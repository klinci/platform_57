<?php

namespace Modules\Integration\Helpers;

use DrewM\MailChimp\MailChimp as MailChimpLib;

class MailChimp
{
    protected $service;

    public function __construct($apiKey)
    {
        $this->service = new MailChimpLib($apiKey);
    }

    public function lists($offset = 0, $count = 500)
    {
        $mailchimpLists = [];
        try {
            $lists = $this->service->get(sprintf('lists?offset=%s&count=%s', $offset, $count));
            if ($lists['total_items'] > 0) {
                $ls = $lists['lists'];
                foreach ($ls as $l) {
                    $mailchimpLists[$l['id']] = $l['name'];
                }
            }
        } catch(\Exception $e) {
            return $e;
        }

        return $mailchimpLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $subscribe = $this->service->post(sprintf('lists/%s/members', $list), [
                'email_address' => $request->email,
                'merge_fields' => [
                    'FNAME' => !is_null($request->firstName) ? $request->firstName : '',
                    'LNAME' => !is_null($request->lastName) ? $request->lastName : ''
                ],
                'status' => 'subscribed'
            ]);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $offset = 0, $count = 500)
    {
        try {
            $contacts = $this->service->get(sprintf('lists/%s/members?offset=%s&count=%s', $list, $offset, $count));
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
