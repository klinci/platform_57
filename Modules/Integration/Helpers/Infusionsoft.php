<?php

namespace Modules\Integration\Helpers;

class Infusionsoft
{
    protected $service;

    public function __construct($service)
    {
        $this->service = $service;
    }

    public function lists()
    {
        $infusionsoftLists = [];
        try {
            $lists = $this->service->getTags();
            foreach ($lists as $list) {
                $infusionsoftLists[$list['id']] = $list['name'];
            }
        } catch (\Exception $e) {
            return $e;
        }
        
        return $infusionsoftLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $subscribe = $this->service->subscribe($list, $request->email, [
                'first_name' => $request->firstName,
                'last_name' => $request->lastName
            ]);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $offset, $limit)
    {
        try {
            $contacts = $this->service->contacts($list, $offset, $limit);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
