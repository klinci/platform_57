<?php

namespace Modules\Integration\Helpers;

use Modules\ActiveCampaign\MailProviderActiveCampaign;

class ActiveCampaign
{
    protected $service;

    public function __construct($url, $apiKey)
    {
        $this->service = new MailProviderActiveCampaign($url, $apiKey);
    }

    public function lists()
    {
        $activecampaignLists = [];
        try {
            $_ACL = $this->service->lists();
            if (!empty($_ACL)) {
                foreach ($_ACL as $list) {
                    $activecampaignLists[$list->id] = $list->name;
                }
            }
        } catch (\Exception $e) {
            return $e;
        }
        
        return $activecampaignLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $ACParams = [
                'first_name' => $request->firstName,
                'last_name' => $request->lastName
            ];

            $subscribe = $this->service->subscribe($list, $request->email, $ACParams);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }

    public function contacts($list, $perPage, $page)
    {
        try {
            $contacts = $this->service->contacts($list, $perPage, $page);
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
