<?php

namespace Modules\Integration\Helpers;

class MarketHero
{
    protected $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function lists()
    {
        $marketheroLists = [];
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => 'http://api.markethero.io']);
            $response = $client->request('GET', '/v1/api/tags?apiKey=' . $this->apiKey);
            $_ = json_decode($response->getBody()->getContents());
            if (isset($_->tags)) {
                if (count($_->tags > 0)) {
                    foreach ($_->tags as $tag) {
                        $marketheroLists[$tag] = $tag;
                    }
                }
            }
        } catch (\Exception $e) {
            return $e;
        }

        return $marketheroLists;
    }

    public function subscribe($list, $request)
    {
        try {
            $p = [
                'apiKey' => $this->apiKey,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'tags' => [$list]
            ];
            $client = new \GuzzleHttp\Client(['base_uri' => 'http://api.markethero.io']);
            $subscribe = $client->request('POST', '/v1/api/tag-lead', ['json' => $p]);
        } catch (\Exception $e) {
            return $e;
        }

        return $subscribe;
    }
}
