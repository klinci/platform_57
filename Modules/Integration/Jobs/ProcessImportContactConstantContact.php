<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\ConstantContact;

class ProcessImportContactConstantContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $paginate;
    protected $token;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $paginate, $token, $importJobID)
    {
        $this->list = $list;
        $this->paginate = $paginate;
        $this->token = $token;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $service = new ConstantContact($this->token);
        $importJob = ImportJob::find($this->importJobID);

        $params = [];
        if (is_integer($this->paginate)) {
            $params['limit'] = $this->paginate;
        } else {
            $params['next'] = $this->paginate;
        }

        try {
            $listMembers = $service->contacts($this->list, $params);
            $contacts = $listMembers->results;
            $total = count($contacts);
            $next = $listMembers->next;

            echo "TOTAL: $total\n";

            if ($total > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo sprintf("ADD %s\n", $contact->email_addresses[0]->email_address);

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => sprintf('%s %s', $contact->first_name, $contact->last_name),
                        'first_name' => $contact->first_name,
                        'last_name' => $contact->last_name,
                        'email' => $contact->email_addresses[0]->email_address,
                        'phone' => $contact->cell_phone,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if (!is_null($next)) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $next,
                    $this->token,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + $total;
            $importJob->update();

            if (is_null($next)) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
