<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\GetResponse;

class ProcessImportContactGetResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $perPage;
    protected $page;
    protected $apiKey;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $perPage, $page, $apiKey, $importJobID)
    {
        $this->list = $list;
        $this->perPage = $perPage;
        $this->page = $page;
        $this->apiKey = $apiKey;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $service = new GetResponse($this->apiKey);
        $importJob = ImportJob::find($this->importJobID);

        try {
            $contacts = $service->contacts($this->list, $this->perPage, $this->page);
            $total = count($contacts);

            echo "TOTAL: $total\n";

            if ($total > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo "ADD $contact->email\n";

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => $contact->name,
                        'first_name' => '',
                        'last_name' => '',
                        'email' => $contact->email,
                        'phone' => isset($contact->phone) ? $contact->phone : '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if ($total == $this->perPage) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $this->perPage,
                    $this->page + 1,
                    $this->apiKey,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + $total;
            $importJob->update();

            if ($total < $this->perPage) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
