<?php

namespace Modules\Integration\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Modules\Integration\Entities\Contact;
use Modules\Integration\Entities\ImportJob;
use Modules\Integration\Helpers\iContact;

class ProcessImportContactiContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;
    protected $offset;
    protected $limit;
    protected $appID;
    protected $appPassword;
    protected $appUsername;
    protected $importJobID;

    public $tries = 2;
    public $timeout = 600;

    public function __construct($list, $offset, $limit, $appID, $appPassword, $appUsername, $importJobID)
    {
        $this->list = $list;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->appID = $appID;
        $this->appPassword = $appPassword;
        $this->appUsername = $appUsername;
        $this->importJobID = $importJobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "START\n";

        $service = new iContact($this->appID, $this->appPassword, $this->appUsername);
        $importJob = ImportJob::find($this->importJobID);

        try {
            $listMembers = $service->contacts($this->list, $this->offset, $this->limit);
            $contacts = $listMembers->contacts;
            $total = $listMembers->total;

            echo "TOTAL: $total\n";

            if ($total > 0) {
                DB::beginTransaction();

                foreach ($contacts as $contact) {
                    echo sprintf("ADD %s\n", $contact->email);

                    Contact::insert([
                        'import_job_id' => $this->importJobID,
                        'full_name' => sprintf('%s %s', $contact->firstName, $contact->lastName),
                        'first_name' => $contact->firstName,
                        'last_name' => $contact->lastName,
                        'email' => $contact->email,
                        'phone' => $contact->phone,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    DB::commit();
                }
            }

            if (($this->offset + $this->limit) < $total) {
                echo "RE-DISPATCH\n";

                $this->dispatch(
                    $this->list,
                    $this->offset + $this->limit,
                    $this->limit,
                    $this->appID,
                    $this->appPassword,
                    $this->appUsername,
                    $this->importJobID
                )->delay(now()->addSeconds(10));
            }

            $importJob->total = $importJob->total + count($contacts);
            $importJob->update();

            if ($importJob->total == $total) {
                $importJob->status = 'COMPLETED';
                $importJob->update();
            }

            echo "FINISH\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
