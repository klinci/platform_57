<?php

namespace Modules\Integration\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImportJob extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'list_id',
        'status',
        'total'
    ];

    protected $table = 'cwa_import_jobs';

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'import_job_id');
    }
}
