<?php

namespace Modules\Integration\Entities;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    protected $table = 'integrations';
    protected $fillable = [
        'name',
        'service',
        'config',
        'workspace_id',
        'user_id'
    ];
}
