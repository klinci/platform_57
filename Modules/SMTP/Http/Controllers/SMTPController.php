<?php

namespace Modules\SMTP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\SMTP\Entities\SMTP;
use Auth;
use JavaScript;

class SMTPController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        JavaScript::put([
            'user' => Auth::user()->toJson(),
            'isAdmin' => Auth::user()->hasRole('admin'),
            'logoDirectory' => env('IMAGE_ASSET_DIRECTORY'),
            'appName' => env('APP_NAME'),
            'scssDirectory' => env('SCSS_ASSET_DIRECTORY')
        ]);

        return view('smtp::index');
    }

    /**
     * List SMTPs
     * @param Object
     * @return SMTPs
     */
    public function smtps()
    {
        return response()->json(SMTP::all(), Response::HTTP_OK);
    }

    /**
     * SMTP
     * @param $smtpId
     * @return SMTP
     */
    public function show($smtpId)
    {
        return response()->json(SMTP::findOrFail($smtpId), Response::HTTP_OK);
    }

    /**
     * Store SMTP
     * @param Object
     * @return SMTP
     */
    public function store(Request $request)
    {
        $smtp = SMTP::create($request->all());

        return response()->json($smtp, Response::HTTP_OK);
    }

    /**
     * Update Campaign
     * @param Object
     * @return Campaign
     */
    public function update(Request $request, $smtpId)
    {
        $smtp = SMTP::findOrFail($smtpId);
        $smtp->update($request->all());

        return response()->json($smtp, Response::HTTP_OK);
    }

    /**
     * Delete Campaign
     * @param Object
     * @return $smtpId
     */
    public function delete($smtpId)
    {
        $smtp = SMTP::findOrFail($smtpId);
        $smtp->delete();

        return null;
    }
}
