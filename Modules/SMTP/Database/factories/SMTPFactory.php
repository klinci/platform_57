<?php

use Faker\Generator as Faker;

$factory->define(Modules\SMTP\Entities\SMTP::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'host' => $faker->domainName,
        'port' => '25',
        'username' => $faker->userName,
        'password' => $faker->password,
        'encryption' => 'tls',
        'from_email' => $faker->email,
        'from_name' => $faker->name
    ];
});
