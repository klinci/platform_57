<?php

use Illuminate\Database\Seeder;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['name' => 'admin', 'guard_name' => 'web'];
        $permissions = [
            [
                'name' => 'user.create',
                'guard_name' => 'web'
            ],
            [
                'name' => 'user.edit',
                'guard_name' => 'web'
            ],
            [
                'name' => 'user.delete',
                'guard_name' => 'web'
            ]
        ];

        DB::table('roles')->insert($roles);
        DB::table('permissions')->insert($permissions);
    }
}
