<?php

use Illuminate\Database\Seeder;

class CampaignTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaignTemplates = [
            [
                'name' => 'Video',
                'slug' => 'video'
            ],
            [
                'name' => 'Quiz',
                'slug' => 'quiz'
            ],
            [
                'name' => 'Mailbot',
                'slug' => 'mailbot'
            ]
        ];

        DB::table('campaign_templates')->insert($campaignTemplates);
    }
}
