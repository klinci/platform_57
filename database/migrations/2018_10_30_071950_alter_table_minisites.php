<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMinisites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->renameColumn('order', 'sort');
            $table->string('next')->nullable();
            $table->integer('campaign_target_id')->nullable();
            $table->json('styles')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minisites', function (Blueprint $table) {
            $table->renameColumn('sort', 'order');
            $table->dropColumn('next');
            $table->dropColumn('campaign_target_id');
            $table->dropColumn('styles');
        });
    }
}
