<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minisites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->string('type')->nullable();
            $table->boolean('first')->nullable();
            $table->boolean('force_redirect')->nullable();
            $table->string('url')->nullable();
            $table->boolean('is_protected_by_optin')->nullable();
            $table->integer('optin')->nullable();
            $table->text('header')->nullable();
            $table->text('content')->nullable();
            $table->text('footer')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minisites');
    }
}
