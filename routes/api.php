<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Hashers\{WorkspaceHasher, TeamHasher, ProjectHasher, CampaignHasher, UserHasher};
use App\Hashers\{MinisiteHasher, WidgetHasher};
use App\{Workspace, Team, Project, Campaign, User, Minisite, Widget};
use Spatie\Permission\Models\{Permission, Role};

Route::middleware('auth:api')->group(function () {
    /**
     * Administrator
     */
    Route::prefix('admin')->group(function () {
        /**
         * User
         */
        Route::get('users', 'API\UserController@list');
        Route::post('user/store', 'API\UserController@store');
        Route::get('user/{user}', 'API\UserController@show');
        Route::put('user/{user}', 'API\UserController@update');
        Route::delete('user/{user}', 'API\UserController@delete');

        /**
         * Access Control
         */
        Route::get('access-control/stats', 'API\AccessControlController@stats');

        /**
         * Access Control Role
         */
        Route::get('access-control/roles', 'API\RoleController@list');
        Route::post('access-control/role/store', 'API\RoleController@store');
        Route::get('access-control/role/{role}', 'API\RoleController@show');
        Route::put('access-control/role/{role}', 'API\RoleController@update');
        Route::delete('access-control/role/{role}', 'API\RoleController@delete');

        /**
         * Access Control Permission
         */
        Route::get('access-control/permissions', 'API\PermissionController@list');
        Route::post('access-control/permission/store', 'API\PermissionController@store');
        Route::get('access-control/permission/{permission}', 'API\PermissionController@show');
        Route::put('access-control/permission/{permission}', 'API\PermissionController@update');
        Route::delete('access-control/permission/{permission}', 'API\PermissionController@delete');

        /**
         * Route Binding
         */
        Route::bind('permission', function (string $id) {
            return Permission::findOrFail($id);
        });

        Route::bind('role', function (string $id) {
            return Role::findOrFail($id);
        });
    });

    /**
     * Profile
     */
    Route::get('profile', 'API\ProfileController@show');
    Route::put('profile', 'API\ProfileController@update');

    /**
     * Workspace
     */
    Route::get('workspaces', 'API\WorkspaceController@list');
    Route::post('workspace', 'API\WorkspaceController@store');
    Route::put('workspace/{workspace}', 'API\WorkspaceController@update');
    Route::delete('workspace/{workspace}', 'API\WorkspaceController@delete');
    Route::get('workspace/{workspace}', 'API\WorkspaceController@show');
    Route::get('workspace/{workspace}/members', 'API\WorkspaceController@members');
    Route::get('workspace/{workspace}/teams', 'API\WorkspaceController@teams');
    Route::delete('workspace/revoke/{workspace}/{user_encoded}', 'API\WorkspaceController@revoke');

    /**
     * Team
     */
    Route::post('workspace/{workspace}/team', 'API\TeamController@store');
    Route::put('team/{team}', 'API\TeamController@update');
    Route::delete('team/{team}', 'API\TeamController@delete');
    Route::get('team/{team}', 'API\TeamController@show');
    Route::get('team/{team}/members', 'API\TeamController@members');
    Route::get('team/{team}/projects', 'API\TeamController@projects');
    Route::delete('team/revoke/{team}/{user_encoded}', 'API\TeamController@revoke');

    /**
     * Project
     */
    Route::post('team/{team}/project', 'API\ProjectController@store');
    Route::put('project/{project}', 'API\ProjectController@update');
    Route::delete('project/{project}', 'API\ProjectController@delete');
    Route::get('project/{project}', 'API\ProjectController@show');
    Route::get('project/{project}/members', 'API\ProjectController@members');
    Route::get('project/{project}/campaigns', 'API\ProjectController@campaigns');
    Route::delete('project/revoke/{project}/{user_encoded}', 'API\ProjectController@revoke');

    /**
     * Campaign
     */
    Route::post('project/{project}/campaign', 'API\CampaignController@store');
    Route::put('campaign/{campaign}', 'API\CampaignController@update');
    Route::delete('campaign/{campaign}', 'API\CampaignController@delete');
    Route::get('campaign/{campaign}', 'API\CampaignController@show')->where('campaign', '<>', 'templates');
    Route::get('campaign/{campaign}/members', 'API\CampaignController@members');
    Route::get('campaign/templates', 'API\CampaignController@templates');
    Route::delete('campaign/revoke/{campaign}/{user_encoded}', 'API\CampaignController@revoke');

    /**
     * Minisite
     */
    Route::get('campaign/{campaign}/minisites', 'API\MinisiteController@list');
    Route::post('campaign/{campaign}/minisite', 'API\MinisiteController@store');
    Route::get('campaign/{campaign}/get-first-minisite', 'API\MinisiteController@getFirstMinisite');
    Route::put('minisite/{minisite}/update-minisite-template', 'API\MinisiteController@updateMinisiteTemplate');

    /**
     * Widget
     */
    Route::get('minisite/{minisite}/widgets', 'API\WidgetController@list');
    Route::post('minisite/{minisite}/widget', 'API\WidgetController@store');
    Route::put('widget/{widget}', 'API\WidgetController@update');
    Route::get('widget/{widget}', 'API\WidgetController@show');

    /**
     * Link
     */
    Route::get('links/{workspace?}', 'API\LinkController@links');

    /**
     * Invitation
     * @ Workspace
     * @ Team
     * @ Project
     * @ Campaign
     */
    Route::post('invitation/send/workspace/{workspace}', 'API\InvitationController@sendInvitationToWorkspace');
    Route::post('invitation/send/team/{team}', 'API\InvitationController@sendInvitationToTeam');
    Route::post('invitation/send/project/{project}', 'API\InvitationController@sendInvitationToProject');
    Route::post('invitation/send/campaign/{campaign}', 'API\InvitationController@sendInvitationToCampaign');

    /**
     * Utilities
     */
    Route::get('utilities/is-first-time-logged-in', 'API\UtilityController@isFirstTimeLoggedIn');
    Route::get('utilities/get-first-workspace', 'API\UtilityController@getFirstWorkspace');
    Route::get('utilities/{workspace}/get-first-team', 'API\UtilityController@getFirstTeam');
    Route::get('utilities/{team}/get-first-project', 'API\UtilityController@getFirstProject');

    /**
     * Route Binding
     */
    Route::bind('workspace', function (string $id) {
        return Workspace::findOrFail(WorkspaceHasher::decode($id));
    });

    Route::bind('team', function (string $id) {
        return Team::findOrFail(TeamHasher::decode($id));
    });

    Route::bind('project', function (string $id) {
        return Project::findOrFail(ProjectHasher::decode($id));
    });

    Route::bind('campaign', function (string $id) {
        return Campaign::findOrFail(CampaignHasher::decode($id));
    });

    Route::bind('user', function (string $id) {
        return User::findOrFail($id);
    });

    Route::bind('user_encoded', function (string $id) {
        return User::findOrFail(UserHasher::decode($id));
    });

    Route::bind('minisite', function (string $id) {
        return Minisite::findOrFail(MinisiteHasher::decode($id));
    });

    Route::bind('widget', function (string $id) {
        return Widget::findOrFail(WidgetHasher::decode($id));
    });
});
