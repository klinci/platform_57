<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * DEV
 */
use App\Invitation;
use App\Hashers\{WorkspaceHasher, InvitationHasher};
use Aws\ElasticTranscoder\ElasticTranscoderClient;

Route::get('t', function () {
    $result = $elasticTranscoder = ElasticTranscoderClient::factory(array(
        'credentials' => array(
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
        ),
        'region' => env('AWS_ET_REGION'), // dont forget to set the region
        'version' => 'latest'
    ));

    //pipeline id 1489836190416-r56a8n
    //dd($elasticTranscoder->listPipelines());

    $elasticTranscoder->createJob([
        //pipeline id refer it in transcoder page eg : https://ap-southeast-2.console.aws.amazon.com/elastictranscoder/home?region=ap-southeast-2#pipelines:
        'PipelineId' => env('AWS_ET_PIPELINE_ID'),
        'Input' => [
            //input file, which should exist in the bucket which is specified on pipeline
            'Key' => 'jellyfish.webm',
            'FrameRate' => 'auto',
            'Resolution' => 'auto',
            'AspectRatio' => 'auto',
            'Interlaced' => 'auto',
            'Container' => 'auto',
        ],
        'Outputs' => [
            [
                //output prefix file name
                'Key' => 'jellyfish.mp4',
                //System preset: HLS Audio - 64k
                
                //Preset id refer it in transcoder page eg : https://ap-southeast-2.console.aws.amazon.com/elastictranscoder/home?region=ap-southeast-2#presets:
                
                'PresetId' => env('AWS_ET_PRESET_ID'),
                'ThumbnailPattern' => 'jellyfish' . '/thumbnails/{count}'
            ]
        ]
    ]);

    dd($result);
});

Route::get('/', function () {
    if (!Auth::check()) {
        return redirect('login');
    }

    $workspaces = Auth::user()->workspaces;

    if ($workspaces->count() > 0) {
        $encoded = WorkspaceHasher::encode($workspaces->first()->workspace->id);

        return redirect(sprintf('workspace/%s', $encoded));
    }

    return redirect('dashboard');
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

/**
 * Locale
 */
Route::get('locale/{locale}', function ($locale) {
    return redirect()->back()->cookie(cookie('locale', $locale, null, '/', null, false, false));
});

/**
 * Handle Invitation
 */
Route::get('invitation/{invitation}', 'InvitationController@handle');
Route::post('invitation/register', 'InvitationController@register');

Route::get('/{any?}', 'AppController@index')->where('any', '^(?!(api|xyz).*$).*');

/**
 * Media Library
 */
Route::get('media-library', 'MediaLibraryController@index');

/**
 * Route Binding
 */
Route::bind('invitation', function (string $id) {
    return Invitation::findOrFail(InvitationHasher::decode($id));
});