<?php

namespace Modules\MadMimi;

class MailProviderMadMimi
{
    private $madmimi;

    public function __construct($email, $apiKey)
    {
        $this->madmimi = new \MadMimi($email, $apiKey);
    }

    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
     * Get MinMimi lists
     */
    public function lists()
    {
        try {
            $aList = array();
            $lists = $this->madmimi->Lists();

            //enable error checking for invalid request
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($lists);
            if (!$xml) {
                return $lists;
            }

            if (isset($xml->list)) {
                if (count($xml->list) > 0) {
                    foreach ($xml->list as $list) {
                        $oList = new \stdClass();
                        $oList->id = $list['id']->__toString();
                        $oList->name = $list['name']->__toString();
                        $oList->subscriber_count = (int)$list['subscriber_count']->__toString();
                        $oList->display_name = $list['display_name']->__toString();

                        $aList[] = $oList;
                    }
                }
            }
            return $this->arrayToObject($aList);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Subscribe new email to MinMimi list
     *
     * @param string $list List ID
     * @param string $email Email address
     * @param associative array $additional Optional. You can add extra data here, such as first_name, last_name, etc.
     * @return string
     */
    public function subscribe($list, $email, $additional = [])
    {
        try {
            $subscribe = $this->madmimi->AddMembership($list, $email, $additional);
            return $subscribe;
        } catch (\Exception $e) {
            return $e;
        }
    }


    /**
     * Unsubscribe email from MadMimi list
     *
     * @param string $list List ID
     * @param string $email Email address
     * @return string
     */
    public function unsubscribe($list, $email)
    {
        try {
            $subscribe = $this->madmimi->RemoveMembership($list, $email);
            return $subscribe;
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function contacts($list, $perPage, $page)
    {
        try {
            $contacts = $this->madmimi->ListSubscribers($list, $perPage, $page);
        } catch (\Exception $e) {
            return $e;
        }

        return json_decode($contacts);
    }
}
