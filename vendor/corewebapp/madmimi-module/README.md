# Core Web App - MadMini Module

## Installation

```bash
composer require corewebapp/madmimi-module 1.0.x-dev
```

## How to Use

```bash
$mailProvider = new MailProviderMadMimi($email, $apiKey);

//get list
$lists = $mailProvider->lists();
dd($lists);

//subscribe
$list_id = 1; //example only, please replace with list id that you can get from $lists
$email = "example@avectris.com";
$mergeVars = [
    'first_name' => 'First',
    'last_name' => 'Last',
];
$mailProvider->subscribe($list_id, $email, $mergeVars);
```
