<?php

namespace Modules\Madmimi\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class MadMimiController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function index()
    {
        return view('madmimi::index');
    }
}
