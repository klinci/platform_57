<?php

Route::group(['prefix' => 'madmimi', 'namespace' => 'Modules\MadMimi\Http\Controllers'], function () {
    Route::get('/', 'MadMimiController@index');
});
