<?php

namespace Modules\Drip;

class MailProviderDrip
{
    private $drip;

    public function __construct($apiKey, $accountID)
    {
        $this->drip = new \Drip\Client($apiKey, $accountID);
    }

    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
    * Get all Drip lists
    */
    public function lists()
    {
        try {
            $lists = $this->arrayToObject($this->drip->get_tags()->get_contents());
            foreach ($lists->tags as $key => $list) {
                $oList = new \stdClass();
                $oList->id = $key;
                $oList->name = $list;

                $aList[] = $oList;
            }
            return $this->arrayToObject($aList);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
    * Subscribe new email to MinMimi list
    *
    * @param string $list List ID
    * @param string $email Email address
    * @param string $firstName Optional. Customer first name
    * @param string $lastName Optional. Customer last name
    * @return string
    */
    public function subscribe($list, $email)
    {
        try {
            $this->drip->create_or_update_subscriber(['email' => $email]);

            return $this->arrayToObject($this->drip->tag_subscriber(['email' => $email, 'tag' => $list]));
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function contacts($list, $page = 1, $perPage = 100)
    {
        try {
            $params = [
                'tags' => $list,
                'page' => $page,
                'per_page' => $perPage
            ];

            $contacts = $this->drip->get_subscribers($params)->get_contents();
        } catch (\Exception $e) {
            return $e;
        }

        return $contacts;
    }
}
