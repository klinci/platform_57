<?php

namespace Modules\Sendlane;

class MailProviderSendlane
{
    protected $apiKey;
    protected $hashKey;
    private $url;

    /**
     * Constructor
     * @param string $apiKey
     * @param string $hashKey
     */
    public function __construct($url, $apiKey, $hashKey)
    {
        $this->apiKey = $apiKey;
        $this->hashKey = $hashKey;
        $this->url = $url . '/api/v1';
    }

    /**
     * Helper to convert array into object
     * @param array $array
     * @return Object
     */
    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
     * Get all sendlane list
     *
     * @return object
     */
    public function lists()
    {
        try {
            $lists = $this->requestList();

            //check is there an error
            if (isset($lists->error)) {
                return $lists->error;
            }

            $aList = [];
            foreach ($lists as $list) {
                $oList = [
                    'id' => $list->list_id,
                    'name' => $list->list_name,
                ];
                $aList[] = $this->arrayToObject($oList);
            }

            return $this->arrayToObject($aList);
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Subcribe new user to list
     *
     * @param integer $list_id List ID
     * @param string $email User Email
     * @param string $name (Optional) Subscriber name
     * @return
     */
    public function subscribe($list_id, $email, $name = '')
    {
        try {
            return $this->addSubscriber($list_id, $email, $name);
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Subcribe new user to list
     *
     * @param integer $list_id List ID
     * @param string $email User Email
     * @return
     */
    public function unsubscribe($list_id, $email)
    {
        try {
            return $this->deleteSubscriber($list_id, $email);
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Get all tag list
     *
     * @return
     */
    public function tags()
    {
        $tags = $this->getTags();
        $lists = [];
        foreach ($tags as $tag) {
            $lists[] = [
                'id' => $tag->tag_id,
                'name' => $tag->tag_name,
            ];
        }
        return $lists;
    }

    /**
     * Set Tags
     *
     * @return
     */
    public function setTags($email, $tag_ids = [])
    {
        $tag_str = '';
        foreach ($tag_ids as $tag_id) {
            $tag_str .= $tag_id . ',';
        }
        $tag_str = substr($tag_str, 0, strlen($tag_str) - 1);

        $data = [
            'api' => $this->apiKey,
            'hash' => $this->hashKey,
            'email' => $email,
            'tag_ids' => $tag_str,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/tag-subscriber-add');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }
    
    /**
     * Remove Tags
     *
     * @return
     */
    public function removeTags($email, $tag_ids = [])
    {
        $tag_str = '';
        foreach ($tag_ids as $tag_id) {
            $tag_str .= $tag_id . ',';
        }
        $tag_str = substr($tag_str, 0, strlen($tag_str) - 1);

        $data = [
            'api' => $this->apiKey,
            'hash' => $this->hashKey,
            'email' => $email,
            'tag_ids' => $tag_str,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/tag-subscriber-remove');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    /**
     * Get all tag list
     *
     * @return
     */
    protected function getTags()
    {
        $data = [
            'api' => $this->apiKey,
            'hash' => $this->hashKey,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/tags');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    /**
     * Request list to Sendlane API
     * @return Output in json format
     */
    protected function requestList()
    {
        $data = [
            'api' => $this->apiKey,
            'hash' => $this->hashKey,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/lists');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    /**
     * Add subscriber to list via Sendlane API
     * @return Output in json format
     */
    protected function addSubscriber($list_id, $email, $name)
    {
        $data = [
            'api' => $this->apiKey,
            'hash' => $this->hashKey,
            'email' => $name . '<' . $email . '>',
            'list_id' => $list_id,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/list-subscribers-add');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    /**
     * Unsubscriber to list via Sendlane API
     * @return Output in json format
     */
    protected function deleteSubscriber($list_id, $email)
    {
        $data = [
            'api' => $this->apiKey,
            'hash' => $this->hashKey,
            'email' => $email,
            'list_id' => $list_id,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/subscribers-delete');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }
}
