<?php

namespace Modules\Sendlane\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Sendlane\MailProviderSendlane;

class SendlaneController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function index()
    {
        // Cleaned up
    }
}
