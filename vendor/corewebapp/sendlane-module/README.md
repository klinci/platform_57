# Core Web App - Sendlane Module

## Installation

```bash
composer require corewebapp/sendlane-module 1.0.x-dev
```

## Usage

```php
<?php

$mailProvider = new MailProviderSendlane($url, $apiKey, $hashKey);

//get list
$lists  = $mailProvider->lists();
dd($lists);

//subscribe
$list   = 1; //example only, please replace with list id that you can get from $lists
$email  = "example@avectris.com";
$name   = "Example Avectris";
$mailProvider->subscribe($list, $email, $name);

//unsubscribe
$mailProvider->unsubscribe($list, $email);
```

## Available Methods

```php
<?php

$mailProvider->lists();

$mailProvider->subscribe($list, $email, $name);

$mailProvider->requestList();

$mailProvider->unsubscribe($list, $email);
```
