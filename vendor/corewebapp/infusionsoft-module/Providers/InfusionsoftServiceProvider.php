<?php

namespace Modules\Infusionsoft\Providers;

use Illuminate\Support\ServiceProvider;

class InfusionsoftServiceProvider extends ServiceProvider
{
    /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
    protected $defer = false;

    /**
    * Boot the application events.
    *
    * @return void
    */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
    }

    /**
    * Register the service provider.
    *
    * @return void
    */
    public function register()
    {

    }

    /**
    * Register config.
    *
    * @return void
    */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('infusionsoft.php'),
        ]);

        $this->mergeConfigFrom(__DIR__.'/../Config/config.php', 'infusionsoft');
    }

    /**
    * Register views.
    *
    * @return void
    */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/infusionsoft');
        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/infusionsoft';
        }, \Config::get('view.paths')), [$sourcePath]), 'infusionsoft');
    }

    /**
    * Register translations.
    *
    * @return void
    */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/infusionsoft');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'infusionsoft');
        }

        $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'infusionsoft');
    }

    /**
    * Get the services provided by the provider.
    *
    * @return array
    */
    public function provides()
    {
        return [];
    }
}
