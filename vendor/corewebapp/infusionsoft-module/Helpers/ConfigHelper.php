<?php

namespace Modules\Infusionsoft\Helpers;

use Modules\Infusionsoft\Config;

class ConfigHelper
{
    public static function config()
    {
        return Config::find(1);
    }

    public static function clientID()
    {
        return self::config()->clientID;
    }

    public static function clientSecret()
    {
        return self::config()->clientSecret;
    }

    public static function redirectURI()
    {
        return self::config()->redirectURI;
    }

    public static function tokenserialized()
    {
        return self::config()->tokenserialized;
    }

    public static function refreshtoken($token)
    {
        $config = Config::find(1);
        $config->tokenserialized = $token;
        $config->save();
    }
}
