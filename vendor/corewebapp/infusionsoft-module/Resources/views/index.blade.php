@extends('core::AdminLTE.app')

@section('htmlheader_title')
Infusionsoft
@endsection

@section('contentheader_title')
Infusionsoft
@endsection

@section('contentheader_description')
Your awesome Infusionsoft integrations
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-shopping-cart"></i> Infusionsoft</a></li>
<li class="active">List</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="#" class="btn btn-primary" data-target="#create-new" data-toggle="modal">
                        <span class="fa fa-plus"></span> Create New
                    </a>
                </h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                    <button data-widget="remove" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if (session('message'))
                    <div class="box-body">
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                            {!! session('message') !!}
                        </div>
                    </div>
                @endif

                <div class="row" style="margin-bottom: 25px;">
                    <div class="col-md-12 text-center">
                        <form action="" method="GET" class="form-inline">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Name"
                                    class="form-control" value="{{ $request->input('name') }}">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="transactiontype">
                                    <option value="">Transaction type</option>
                                    <option value="sale" {{ $request->input('transactiontype') == 'sale' ? ' selected' : '' }}>
                                        Sale
                                    </option>
                                    <option value="refund" {{ $request->input('transactiontype') == 'refund' ? ' selected' : '' }}>
                                        Refund
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="JVZoo Product ID"
                                    class="form-control" name="jvzoo_id" value="{{ $request->input('jvzoo_id') }}">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Infusionsoft Tag" class="form-control"
                                    name="tag" value="{{ $request->input('tag') }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-trash-o"></i> Search
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table id="infusionsoft_datatables" class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Transaction Type</th>
                                    <th>JVZoo Product ID</th>
                                    <th>Infusionsoft Tag</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($infusionsofts as $infusionsoft)
                                    <tr>
                                        <td>{{ $infusionsoft->name }}</td>
                                        <td>{{ $infusionsoft->transactiontype }}</td>
                                        <td>{{ $infusionsoft->jvzoo_id }}</td>
                                        <td>{{ $infusionsoft->tag }}</td>
                                        <td>
                                            <form action="{{ url('cwa/infusionsoft/'.$infusionsoft->uniqueid()) }}"
                                                method="post" style="display:inline;">
                                                {!! csrf_field() !!}
                                                {!! method_field('DELETE') !!}
                                                <button type="submit" class="btn btn-danger btn-xs"
                                                    onclick="return confirm('Are you sure want to trash this item?')" title="Trash">
                                                    <i class="fa fa-trash-o"></i> Trash
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="float: right">
                            {!! $infusionsofts->appends(['name' => $request->input('name'), 'transactiontype' => $request->input('transactiontype'), 'jvzooId' => $request->input('jvzooId'), 'tag' => $request->input('tag')])->render() !!}
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal_per_page')
<!-- Modal -->
<div class="modal fade" id="create-new" tabindex="-1" role="dialog" aria-labelledby="createNewInfusionsoft">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="createNewInfusionsoft">Create new Infusionsoft</h4>
            </div>
            <form role="form" method="POST" action="{{ route('infusionsoft.store') }}">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" placeholder="Name" class="form-control" id="name" />
                    </div>
                    <div class="form-group">
                        <label for="transactiontype">Transaction Type</label>
                        <select name="transactiontype" placeholder="Transaction Type" class="form-control" id="transactiontype">
                            <option value="sale">Sale</option>
                            <option value="refund">Refund</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jvzoo_id">JVZoo Product ID</label>
                        <input type="text" name="jvzoo_id" placeholder="JVZoo Product ID" class="form-control" id="jvzoo_id" />
                    </div>
                    <div class="form-group">
                        <label for="tag">Infusionsoft Tag</label>
                        <input type="text" name="tag" placeholder="Infusionsoft Tag" class="form-control" id="tag" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="fa fa-save"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('javascript_per_page')
<script type="text/javascript">
    $(function(){
        /**
         * @function Load infusionsofts
         */
        try {
            $('#infusionsoft_datatables')
            .dataTable({
                "aoColumns" : [
                    {},
                    {sClass:"datatable_align_center"},
                    {sClass:"datatable_align_center"},
                    {sClass:"datatable_align_center"},
                    {bSortable: false, sClass:"datatable_align_center"}
                ],
                "bFilter" : false
            });
        } catch (err) {
            showTemporaryMessage('Could not load data, please try to refresh the page.', 'error', 5);
        }
    });
</script>
@endsection
