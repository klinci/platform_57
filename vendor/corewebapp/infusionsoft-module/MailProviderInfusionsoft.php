<?php

namespace Modules\Infusionsoft;

use Infusionsoft\Infusionsoft;

class MailProviderInfusionsoft
{
    protected $clientId;
    protected $clientSecret;
    protected $redirectUri;
    protected $infusionsoft;

    /**
     * Constructor
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     */
    public function __construct($clientId, $clientSecret, $redirectUri)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUri = $redirectUri;

        $this->infusionsoft = new Infusionsoft(array(
            'clientId'     => $this->clientId,
            'clientSecret' => $this->clientSecret,
            'redirectUri'  => url($this->redirectUri),
        ));
    }
    
     /**
     * Set token only
     *
     * @param string $token
     */
    public function setToken($token)
    {
        $token = unserialize($token);
        $this->infusionsoft->setToken($token);
    }
    /**
     * Set and refresh token
     *
     * @param string $token
     * @return string new token
     */
    public function setAndRefreshToken($token)
    {
        $token = unserialize($token);

        $this->infusionsoft->setToken($token);
        $this->infusionsoft->refreshAccessToken();

        $newToken = serialize($this->infusionsoft->getToken());

        return $newToken;
    }

    /**
     * Request token with code from authentication
     *
     * @param string $code
     * @return string Token
     */
    public function requestToken($code)
    {
        if (!empty($code) && !$this->infusionsoft->getToken()) {
            $this->infusionsoft->requestAccessToken($code);
        }

        return $this->infusionsoft->getToken();
    }

    /**
     * Get authorize URL
     * @return string URL
     */
    public function getAuthorizationUrl()
    {
        return $this->infusionsoft->getAuthorizationUrl();
    }

    /**
     * Subcribe new user to list
     *
     * @param integer $tag_id Tag ID
     * @param string $email User Email
     * @param associative array $params (Optional) Subscriber name
     * @return
     */
    public function subscribe($tag, $email, $params = [])
    {
        try {
            try {
                $contact = $this->infusionsoft->contacts()->where('email', $email)->first();
            } catch (\Infusionsoft\InfusionsoftException $ex) {
                $firstName = isset($params['first_name']) ? $params['first_name'] : '';
                $lastName = isset($params['last_name']) ? $params['last_name'] : '';

                $e = new \stdClass;
                $e->field = 'EMAIL1';
                $e->email = $email;
                $name = $firstName . ' ' . $lastName;
                $c = ['given_name' => $name, 'email_addresses' => [$e]];

                $contact = $this->infusionsoft->contacts()->create($c);
            }

            $g = new \Infusionsoft\Api\ContactService($this->infusionsoft);
            $g->addToGroup($contact->id, $tag);

            return $contact;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getTags()
    {
        $data = [];
        $query = ['GroupName' => '%'];
        $returnFields = ['Id', 'GroupName'];
        $tags = $this->infusionsoft->data()->query("ContactGroup", 1000, 0, $query, $returnFields, "Id", true);

        foreach ($tags as $tag) {
            $data[] = [
                "id" => $tag["Id"],
                "name" => $tag["GroupName"],
            ];
        }

        return $data;
    }

    public function isTokenExpired()
    {
        return $this->infusionsoft->isTokenExpired();
    }

    public function contacts($list, $offset, $limit)
    {
        try {
            $contacts = $this->infusionsoft->tags()->contacts($list, $offset, $limit);
        } catch (\Infusionsoft\InfusionsoftException $e) {
            return $e;
        }

        return $contacts;
    }
}
