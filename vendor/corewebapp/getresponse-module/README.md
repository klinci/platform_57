# Core Web App - GetResponse Module

## Installation

```bash
composer require corewebapp/getresponse-module 1.0.x-dev
```

## How to Use

```bash
$mailProvider = new MailProviderGetResponse($getResponseApiKey);

//get list
$lists = $mailProvider->lists();
dd($lists);

//subscribe
$list_id = 1; //example only, please replace with list id that you can get from $lists
$email = "example@avectris.com";
$name = "Example Avectris";
$mailProvider->subscribe($list_id, $email, $name);

//unsubscribe
$mailProvider->unsubscribe($email);
```
