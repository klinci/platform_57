<?php

namespace Modules\ActiveCampaign;

use Illuminate\Database\Eloquent\Model;
use \ActiveCampaign;

class MailProviderActiveCampaign extends Model
{
    private $activecampaign;

    /**
     * ActiveCampaign
     * @param $apiUrl, $apiKey
     */
    public function __construct($apiUrl, $apiKey)
    {
        $this->activecampaign = new ActiveCampaign($apiUrl, $apiKey);
    }

    /**
     * Get account
     */
    public function account()
    {
        try {
            $account = $this->activecampaign->api('account/view');
            return $account;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Convert array to object
     */
    private function arrayToObject($array)
    {
        return (object)$array;
    }

    /**
     * Get lists
     */
    public function lists()
    {
        try {
            $aList = [];
            $aLists = $this->activecampaign->api('list/list?full=0&ids=all');
			
            $lists = $this->arrayToObject($aLists);

            if ($lists->result_code) {
                foreach ($lists as $key => $list) {
                    if (is_numeric($key)) {
                        $oList = new \stdClass();
                        $oList->id = $list->id;
                        $oList->name = $list->name;
                        $aList[] = $oList;
                    }
                }
            }
            return $this->arrayToObject($aList);
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Get subscribers
     * @param @list
     */
    public function contacts($list, $perPage = 100, $page = 1)
    {
        try {
            $aContact = new \stdClass();

            $endpoint = sprintf('contact/list?full=1&filters[listid]=%s&per_page=%s&page=%s', $list, $perPage, $page);
            $subscribers = $this->activecampaign->api($endpoint);

            if ($subscribers->result_code) {
                foreach ($subscribers as $key => $contact) {
                    if (is_numeric($key)) {
                        $aContact->contacts[] = $contact;
                    }
                }
            } else {
                $aContact->contacts = null;
            }

            $aContact->total = count($aContact->contacts);
            return $aContact;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Subscribe email to list
     * @param integer $list list id
     * @param string $email subscriber email
     * @param associative array $post optional you can add more data here such as first_name, last_name, phone, etc.
     * See more at http://www.activecampaign.com/api/example.php?call=contact_sync
     */
    public function subscribe($list, $email, $post = [], $tags = [])
    {
        try {
            $contact = [
                'email' => $email,
                'p[' . $list . ']' => $list,
                'status[' . $list . ']' => 1
            ];

            if (!empty($tags)) {
                $contact['tags'] = implode(',', $tags);
            }

            $contact = array_merge($contact, $post);

            $subscribe = $this->activecampaign->api('contact/sync', $contact);
            return $subscribe;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Unsubscrie email from list
     * @param $list, $contactid
     */
    public function unsubscribe($list, $contactid)
    {
        try {
            $unsubscribe = $this->activecampaign->api('contact/delete?id='.$contactid.'&listids['.$list.']='.$list);
            return $unsubscribe;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function addTags($email, $tags)
    {
        try {
            $tag = $this->activecampaign->api('contact/tag/add?=', ['email' => $email, 'tags' => $tags]);
            return $tag;
        } catch (Exception $e) {
            return $e;
        }
    }
	
	public function removeTags($email, $tags)
    {
        try {
            $tag = $this->activecampaign->api('contact/tag/remove?=', ['email' => $email, 'tags' => $tags]);
            return $tag;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getTags()
    {
        try {
            $tags = $this->activecampaign->api('tags/list', ['api_output' => 'json']);
            $tags;
            if (!is_array($tags)) {
                return $tags;
            }
            $lists = $this->arrayToObject($tags);
            return $lists;
        } catch (Exception $e) {
            return $e;
        }
    }
}
