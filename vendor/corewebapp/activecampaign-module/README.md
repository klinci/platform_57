# Core Web App - ActiveCampaign Module

Installation

```bash
composer require corewebapp/activecampaign-module 1.0.x-dev
```

## How to Use

```bash
$mailProvider = new MailProviderActiveCampaign($apiUrl, $apiKey);

//get list
$lists = $mailProvider->lists();
dd($lists);

//subscribe
$list_id = 1; //example only, please replace with list id that you can get from $lists
$email = "example@avectris.com";
$mergeVars = [
    'first_name' => 'First',
    'last_name' => 'Last',
];
$mailProvider->subscribe($list_id, $email, $mergeVars);
```
