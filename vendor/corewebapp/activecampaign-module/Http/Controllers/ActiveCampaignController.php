<?php

namespace Modules\Activecampaign\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\ActiveCampaign\MailProviderActiveCampaign;

class ActiveCampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function index()
    {
        return view('activecampaign::index');
    }
}
