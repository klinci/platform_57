<?php

Route::group(['prefix' => 'activecampaign', 'namespace' => 'Modules\ActiveCampaign\Http\Controllers'], function () {
    Route::get('/', 'ActiveCampaignController@index');
});
