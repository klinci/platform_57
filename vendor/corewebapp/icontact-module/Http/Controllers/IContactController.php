<?php

namespace Modules\Icontact\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class IContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function index()
    {
        return view('icontact::index');
    }
}
