<?php

Route::group(['prefix' => 'aweber', 'namespace' => 'Modules\Aweber\Http\Controllers'], function () {
    Route::get('/', 'AweberController@index');
});
